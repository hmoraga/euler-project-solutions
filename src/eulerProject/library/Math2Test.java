package eulerProject.library;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Math2Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testIsTruncatablePrimeTrue() {
		boolean result = Math2.isTruncatablePrime(3797);
		assertTrue(result);
	}

}
