package eulerProject.library;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class String2Test {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetBinary() {
		int num = 34;
		String numStr = String2.getBinary(num);
		
		assertEquals("100010", numStr);
	}
	
	@Test
	public void testIsPalindromeTrue() {
		boolean resultA = String2.isPalindrome("10011001");
		boolean resultB = String2.isPalindrome("1101011");
		assertTrue(resultA);
		assertTrue(resultB);
	}

	@Test
	public void testIsPalindromeFalse() {
		boolean resultA = String2.isPalindrome("1011001");
		boolean resultB = String2.isPalindrome("1101001");
		assertFalse(resultA);
		assertFalse(resultB);
	}
}
