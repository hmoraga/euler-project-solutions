package eulerProject.library;

public class Pair<U extends Comparable<U>,V extends Comparable<V>> {
	protected U a;
	protected V b;
	
	public Pair(U a, V b){
		this.a = a;
		this.b = b;
	}
	
	public U getBase() {
		return a;
	}
	
	public V getExp() {
		return b;
	}
	
	public U getFirst() {
		return a;
	}
	public V getSecond() {
		return b;
	}

	@Override
	public String toString() {
		return "(" + a + ", " + b + ")";
	}

	//public abstract int compareTo(Pair<U,V> p1);
}
