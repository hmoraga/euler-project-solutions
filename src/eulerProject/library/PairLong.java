package eulerProject.library;

public class PairLong extends Pair<Long, Long> {

	public PairLong(Long a, Long b) {
		super(a, b);
	}

	@Override
	public String toString() {
		return "("+a+","+b+")";
	}
}
