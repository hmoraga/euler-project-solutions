package eulerProject.library;

public class PairInteger extends Pair<Integer, Integer> implements Comparable<PairInteger>{

	public PairInteger(Integer a, Integer b) {
		super(a, b);
	}

	@Override
	public int compareTo(PairInteger o) {
		Integer prod = getFirst()*getSecond();
		Integer oProd = o.getFirst()*o.getSecond();
		
		return prod.compareTo(oProd);
	}

	@Override
	public String toString() {
		return "PairInteger [" + this.getFirst() + "," + this.getSecond() + "]";
	}

	
}
