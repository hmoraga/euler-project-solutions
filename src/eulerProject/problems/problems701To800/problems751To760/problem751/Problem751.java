package eulerProject.problems.problems701To800.problems751To760.problem751;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class Problem751 {

	public static void main(String[] args) {
		List<Integer> a = new ArrayList<>();
		List<BigDecimal> b = new ArrayList<>();
		AB x = new AB(2, new BigDecimal(2));
		
		a.add(x.getA()); //a1
		b.add(x.getB());
		int currentNumber = 2;
		
		do {
			int idx = currentNumber;
			BigDecimal bi;
			int ai;
			
			do {
				ai = idx;
				bi = concatenar(a,ai);
				a.add(ai);
				b.add(bi);
				
				x = getNewStep(b,a);
				
				if (x.getA()==ai) 
				{
					currentNumber=idx;
					break;
				}
				else {
					a.remove(a.size()-1);
					b.remove(b.size()-1);
					idx++;
				}
			} while (x.getA()!=ai);
		} while (b.get(b.size()-1).toPlainString().length()<=26);
	}

	private static BigDecimal concatenar(List<Integer> a, int ai) {
		if (a.isEmpty()) {
			return new BigDecimal(ai);
		} else {
			StringBuilder str = new StringBuilder();
			
			for (int i=0;i<a.size();i++) {
				str=str.append(a.get(i));
				
				if (i==0) {
					str=str.append(".");
				}
			}
			str = str.append(ai);
			
			return new BigDecimal(str.toString());
		}
	}

	private static AB getNewStep(List<BigDecimal> bi, List<Integer> ai) {
		int lastA = ai.get(ai.size()-1);
		BigDecimal lastB = bi.get(bi.size()-1);
		
		BigDecimal b = new BigDecimal(lastA).multiply(lastB.subtract(new BigDecimal(lastA)).add(BigDecimal.ONE));
		int a = b.toBigInteger().intValue();
		
		return new AB(a, b);
	}

	private static class AB {
		Integer a;
		BigDecimal b;
		
		public AB(Integer a, BigDecimal b) {
			this.a = a;
			this.b = b;
		}
		
		public Integer getA() {
			return a;
		}
		public void setA(Integer a) {
			this.a = a;
		}
		public BigDecimal getB() {
			return b;
		}
		public void setB(BigDecimal b) {
			this.b = b;
		}
	}
}
