package eulerProject.problems.problems1To100.problems11To20.problem20;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class Problem20Test {
	private Problem20 p20;
	
	@Before
	public void setUp() throws Exception {
		p20 = new Problem20();
	}

	@Test
	public void testExecute() {
		p20.setNum(100);
		p20.execute();
		int sum = p20.getSum();
		assertEquals(648, sum);
	}

	@Test
	public void testExample() {
		p20.setNum(10);
		p20.execute();
		int sum = p20.getSum();
		assertEquals(27, sum);
	}

	@Test
	public void testFactorial() {
		BigInteger x = p20.factorial(BigInteger.TEN);
		assertEquals(new BigInteger("3628800"), x);
	}

}
