package eulerProject.problems.problems1To100.problems11To20.problem20;

import java.math.BigInteger;

public class Problem20 {
	private int num;
	private int sum;
	
	public void execute() {
		BigInteger fact = factorial(new BigInteger(String.valueOf(num)));
		
		String bigNumStr = fact.toString();
		
		for (int i=0;i<bigNumStr.length();i++) {
			sum+=Integer.parseInt(bigNumStr.substring(i, i+1));
		}
	}
	
	public BigInteger factorial (BigInteger n) {
		if (n.longValue()<=1L) {return BigInteger.ONE;}
		else {
			return n.multiply(factorial(n.subtract(BigInteger.ONE)));
		}
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}
}
