package eulerProject.problems.problems1To100.problems11To20.problem16;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class Problem16Test {
	private Problem16 p16;
	
	@Before
	public void setUp() throws Exception {
		p16 = new Problem16();
	}

	@Test
	public void testExecute() {
		p16.setNum(BigInteger.valueOf(2).pow(1000));
		p16.execute();
		
		long digitsSum = p16.getSum();
		assertEquals(1366L, digitsSum);
	}

	@Test
	public void testExample() {
		p16.setNum(BigInteger.valueOf(2).pow(15));
		p16.execute();
		
		long digitsSum = p16.getSum();
		assertEquals(26L, digitsSum);
	}
	
}
