package eulerProject.problems.problems1To100.problems11To20.problem16;

import java.math.BigInteger;

public class Problem16 {
	private BigInteger num;
	private long sum;
	
	public void execute() {
		String numStr = num.toString();
		sum = 0;
		
		for (int i=0; i<numStr.length();i++) {
			String digit = numStr.substring(i,i+1);
			sum += Long.parseLong(digit);
		}
	}

	public long getSum() {
		return sum;
	}

	public void setSum(long sum) {
		this.sum = sum;
	}

	public BigInteger getNum() {
		return num;
	}

	public void setNum(BigInteger num) {
		this.num = num;
	}
}
