package eulerProject.problems.problems1To100.problems11To20.problem14;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class Problem14 {
	private Map<Long, Integer> memoization = new HashMap<>();
	private int num;
	private long keyNumber;
	private int longRun;
	
	public void execute() {
		long n=1;
		
		while (n<num) {
			memoization.put(n, calculateCollatzLongitude(n));
			n++;
		}
	
		keyNumber = Collections.max(memoization.entrySet(), Comparator.comparingInt(Map.Entry::getValue)).getKey();
		longRun = memoization.get(keyNumber);
		//System.out.println("La secuencia de Collatz mas larga se da con el número " + biggestCollatz + ":" + biggestCollatzChain + " números");
	}

	public int calculateCollatzLongitude(long n) {
		if (n==1) {
			return 1;
		} else if (memoization.containsKey(n)) {
			return memoization.get(n);
		} else if (n%2==0) {
			return 1+calculateCollatzLongitude(n/2);
		} else {
			return 1+calculateCollatzLongitude(3*n+1);
		}
	}

	public long getKeyNumber() {
		return keyNumber;
	}

	public void setKeyNumber(long keyNumber) {
		this.keyNumber = keyNumber;
	}

	public int getLongRun() {
		return longRun;
	}

	public void setLongRun(int longRun) {
		this.longRun = longRun;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}
}
