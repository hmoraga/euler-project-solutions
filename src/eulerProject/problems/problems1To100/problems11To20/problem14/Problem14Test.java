package eulerProject.problems.problems1To100.problems11To20.problem14;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem14Test {
	private Problem14 p14;
	
	@Before
	public void setUp() throws Exception {
		p14 = new Problem14();
	}

	@Test
	public void testExecute() {
		p14.setNum(1000000);
		
		p14.execute();
		long key = p14.getKeyNumber();
		int longRun = p14.getLongRun();
		
		assertEquals(837799, key);
		assertEquals(525, longRun);
	}

	@Test
	public void testExample() {
		p14.setNum(10);
		
		p14.execute();
		long key = p14.getKeyNumber();
		int longRun = p14.getLongRun();
		
		assertEquals(9, key);
		assertEquals(20, longRun);
	}

	@Test
	public void testCalculateCollatzLongitude() {
		int longRun = p14.calculateCollatzLongitude(13);
		assertEquals(10, longRun);
	}

}
