package eulerProject.problems.problems1To100.problems11To20.problem19;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Problem19 {
	private int totalDays;
	private int sundaysFirstDayOfTheMonthQty;
	private List<Integer> yearsList = new ArrayList<>(100);
	private List<Integer> leapYearsList = new ArrayList<>(25);
	private List<Integer> firstDays = new ArrayList<>(12);
	private List<Integer> firstDaysLeapYears = new ArrayList<>(12);
	private List<Integer> firstDaysList = new ArrayList<>(1200);
	private List<Integer> sundaysList = new ArrayList<>(5210);
	private List<Integer> result = new ArrayList<>(1200);
	
	public void execute() {
		// years and leap-years
		fillYearsAndLeapYears();
		
		// first days of a month between the year count
		Integer[] firstDaysArray = new Integer[]{1,32,60,91,121,152,182,213,244,274,305,335};
		firstDays.addAll(Arrays.asList(firstDaysArray));
		Integer[] firstDaysLeapYear = new Integer[]{1,32,61,92,122,153,183,214,245,275,306,336};
		firstDaysLeapYears.addAll(Arrays.asList(firstDaysLeapYear));
		
		// total of days of between the period (01/01/1901 to 12/31/2000)
		calculateTotalDays();
		// calculate number of sundays
		calculateSundaysQty();

		// calculate first days of each month through the years
		calculateFirstDaysList();

		// check the list of sundays compared with the first days of each month,
		// when there is a coincidence they will be the first days of the month
		sundaysFirstDayOfTheMonthQty = calculateSundaysFirstDayOfMonth();
	}

	private int calculateSundaysFirstDayOfMonth() {
		for (Integer diaDomingo: sundaysList) {
			if (firstDaysList.contains(diaDomingo)) {
				result.add(diaDomingo);
			}
		}
		return result.size();
	}

	private void calculateFirstDaysList() {
		for (Integer anio : yearsList) {
			if (leapYearsList.contains(anio)) {
				for (Integer primerdia : firstDaysLeapYears) {
					primerdia+=(365*nonLeapYearsBetweenTwoYears(yearsList.get(0), anio)
							+366*leapYearsBetweenTwoYears(yearsList.get(0), anio));
					firstDaysList.add(primerdia);
				}
			} else {
				for (Integer primerdia : firstDays) {
					primerdia+=(365*nonLeapYearsBetweenTwoYears(yearsList.get(0), anio)
							+366*leapYearsBetweenTwoYears(yearsList.get(0), anio));
					firstDaysList.add(primerdia);
				}
			}
		}
	}

	private void calculateSundaysQty() {
		// sunday days list
		for (int k=0;7*k+6<=totalDays;k++) {
			sundaysList.add(7*k+6);
		}
	}

	private void calculateTotalDays() {
		totalDays = 0;
		
		for (int anio=1901;anio<=2000;anio++) {
			totalDays+=365;

			if (isLeapYear(anio)) {
				totalDays+=1;
			}
		}
	}

	private void fillYearsAndLeapYears() {
		for (int i=1901;i<=2000;i++) {
			yearsList.add(i);
			if (isLeapYear(i)) {
				leapYearsList.add(i);
			}
		}
	}

	private boolean isLeapYear(int anio) {
		return (anio%4==0);
	}
	
	/**
	 * Sirve para calcular el desfase de los primeros dias del mes
	 * @param anioIni se incluye año de inicio (minimo 1901)
	 * @param anioFin no se incluye (maximo 2000)
	 * @return
	 */
	private int leapYearsBetweenTwoYears(int anioIni, int anioFin) {
		int cont = 0;
		
		for (int j=anioIni;j<anioFin;j++) {
			if (isLeapYear(j)) {
				cont++;
			}
		}
		
		return cont;
	}
	
	/**
	 * Sirve para calcular el desfase de los primeros dias del mes
	 * @param anioIni se incluye año de inicio (minimo 1901)
	 * @param anioFin no se incluye (maximo 2000)
	 * @return
	 */
	private int nonLeapYearsBetweenTwoYears(int anioIni, int anioFin) {
		return anioFin-anioIni-leapYearsBetweenTwoYears(anioIni, anioFin);
	}

	public int getSundaysFirstDayOfTheMonthQty() {
		return sundaysFirstDayOfTheMonthQty;
	}

	public void setSundaysFirstDayOfTheMonthQty(int sundaysFirstDayOfTheMonthQty) {
		this.sundaysFirstDayOfTheMonthQty = sundaysFirstDayOfTheMonthQty;
	}
}
