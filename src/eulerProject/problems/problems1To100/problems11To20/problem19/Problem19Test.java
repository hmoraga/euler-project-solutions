package eulerProject.problems.problems1To100.problems11To20.problem19;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem19Test {
	private Problem19 p19;
	
	@Before
	public void setUp() throws Exception {
		p19 = new Problem19();
	}

	@Test
	public void testExecute() {
		p19.execute();
		int result = p19.getSundaysFirstDayOfTheMonthQty();
		assertEquals(171, result);
	}

}
