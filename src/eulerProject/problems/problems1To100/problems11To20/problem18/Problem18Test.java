package eulerProject.problems.problems1To100.problems11To20.problem18;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class Problem18Test {
	private Problem18 p18;
	
	@Before
	public void setUp() throws Exception {
		p18 = new Problem18();
	}

	@Test
	public void testExecute() throws FileNotFoundException {
		p18.setFilename("triangulo15.txt");
		p18.execute();
		int sum = p18.getSum();
		assertEquals(1074, sum);
	}

	@Test
	public void testExample() throws FileNotFoundException {
		p18.setFilename("triangulo4.txt");
		p18.execute();
		int sum = p18.getSum();
		assertEquals(23, sum);
	}

}
