package eulerProject.problems.problems1To100.problems11To20.problem18;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Scanner;

public class Problem18 {
	private int sum;
	private String filename;
	
	public void execute() throws FileNotFoundException {
		int[][] arrTriangular = fillArray(filename);
		
		// I change the array in a binary tree where each node has two values (row, index) and (key) and his value
		// then i implement a recursion algorithm of max sum of a branch, to select finally wich one is the
		// one with the maximum sum.
		sum=findMaxSum(arrTriangular, 0, 0);
		//System.out.println("La suma máxima es:" + suma);
	}

	private int[][] fillArray(String filename) throws FileNotFoundException {
		String userDirectory = Paths.get("")
                .toAbsolutePath()
                .toString();
		// calculus of n
		int n = Integer.parseInt(filename.substring(9, filename.indexOf(".txt")));

		int[][] triangularArray = new int[n][n];
		// I read the data from the file
		File file = new File(userDirectory+"/src/eulerProject/problems/problems1To100/problems11To20/problem18/"+filename);
		Scanner sc = new Scanner(file);
		int row = 0;
		
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			line=line.trim();
			
			if (!line.isEmpty()) {
				String[] arrStr = line.split(" ");

				for (int i = 0; i < arrStr.length; i++) {
					String numStr = arrStr[i];
					triangularArray[row][i] = Integer.parseInt(numStr);
				}
				row++;
			}	
		}

		sc.close();
		
		return triangularArray;
	}
	
	private int findMaxSum(int[][] triangularArray, int row, int column) {
		int maxCol = row;
		int maxRow = triangularArray.length; // row qty of the array
		
		// border cases
		if (row==maxRow-1) {
			if (column<=maxCol) {
				return triangularArray[row][column];
			} else {
				return 0;
			}
		} else if (row<maxRow-1){
			if (column<=maxCol) {
				return triangularArray[row][column]
						+Math.max(findMaxSum(triangularArray, row+1, column), findMaxSum(triangularArray, row+1, column+1));
			} else {
				return 0;
			}
		} else {
			return 0;			
		}
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
