package eulerProject.problems.problems1To100.problems11To20.problem15;

import static org.junit.Assert.*;

import java.math.BigInteger;

import org.junit.Before;
import org.junit.Test;

public class Problem15Test {
	private Problem15 p15;
	
	@Before
	public void setUp() throws Exception {
		p15 = new Problem15();
	}

	@Test
	public void testExecute() {
		p15.setN(new BigInteger("20"));
		p15.execute();
		
		BigInteger num = p15.getPermutacion();
		assertEquals("137846528820", num.toString());
	}

	@Test
	public void testExample1() {
		p15.setN(new BigInteger("1"));
		p15.execute();
		
		BigInteger num = p15.getPermutacion();
		assertEquals("2", num.toString());
	}
	
	@Test
	public void testExample2() {
		p15.setN(new BigInteger("2"));
		p15.execute();
		
		BigInteger num = p15.getPermutacion();
		assertEquals("6", num.toString());
	}

	@Test
	public void testExample3() {
		p15.setN(new BigInteger("3"));
		p15.execute();
		
		BigInteger num = p15.getPermutacion();
		assertEquals("20", num.toString());
	}
	
}
