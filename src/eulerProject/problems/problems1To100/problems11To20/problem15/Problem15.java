package eulerProject.problems.problems1To100.problems11To20.problem15;

import java.math.BigInteger;

import eulerProject.library.Math2;

public class Problem15 {
	private BigInteger n;
	private BigInteger permutacion;

	public BigInteger getN() {
		return n;
	}

	public void setN(BigInteger n) {
		this.n = n;
	}
	
	public BigInteger getPermutacion() {
		return permutacion;
	}

	public void setPermutacion(BigInteger permutacion) {
		this.permutacion = permutacion;
	}

	public void execute() {
		//n = BigInteger.valueOf(20);
		// El problema corresponde a una permitacion con repeticion
		// los desplazamientos son de 2 tipos: R(right) y D(down)
		// para un cuadrado de 1x1, los movimientos son 2,
		// para un cuadrado de 2x2, los movimientos son 4,
		// para un cuadrado de 3x3, los movimientos son 6,...
		// para un cuadrado de nxn, los movimientos son 2n
		permutacion = Math2.factorial(n.multiply(BigInteger.valueOf(2))).divide((Math2.factorial(n).multiply(Math2.factorial(n))));
	}
	
}
