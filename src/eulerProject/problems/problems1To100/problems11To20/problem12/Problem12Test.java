package eulerProject.problems.problems1To100.problems11To20.problem12;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem12Test {
	private Problem12 p12;
	
	@Before
	public void setUp() throws Exception {
		p12 = new Problem12();		
	}

	@Test
	public void testExecute() {
		p12.setDivisorsQty(500);
		p12.execute();
		long num = p12.getNum();
		assertEquals(76576500, num);
	}

	@Test
	public void testExample() {
		p12.setDivisorsQty(5);
		p12.execute();
		long num = p12.getNum();
		assertEquals(28, num);
	}

}
