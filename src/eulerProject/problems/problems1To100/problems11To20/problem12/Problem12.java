package eulerProject.problems.problems1To100.problems11To20.problem12;

import eulerProject.library.Math2;

public class Problem12 {
	private int divisorsQty;
	private long num;
	
	public void execute() {
		long n=1L;
		int localDivisorsQty=0;
				
		do {
			// se genera el numero triangular
			long tri = Math2.triangular(n); 
			
			localDivisorsQty = Math2.divisores(tri).size();
			
			if (localDivisorsQty>divisorsQty) {
				num = tri;
				break;
			}
			
			n++;
		} while (localDivisorsQty <= divisorsQty);
	}

	public int getDivisorsQty() {
		return divisorsQty;
	}

	public void setDivisorsQty(int divisorsQty) {
		this.divisorsQty = divisorsQty;
	}

	public long getNum() {
		return num;
	}

	public void setNum(long num) {
		this.num = num;
	}	
}
