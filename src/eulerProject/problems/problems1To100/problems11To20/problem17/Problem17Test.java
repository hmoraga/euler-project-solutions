package eulerProject.problems.problems1To100.problems11To20.problem17;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem17Test {
	private Problem17 p17;
	
	@Before
	public void setUp() throws Exception {
		p17 = new Problem17();
	}

	@Test
	public void testExecute() {
		p17.execute();
		int sum = p17.getSum();
		assertEquals(21124, sum);
	}

}
