package eulerProject.problems.problems1To100.problems11To20.problem13;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem13Test {
	private Problem13 p13;
	
	@Before
	public void setUp() throws Exception {
		p13 = new Problem13();
	}

	@Test
	public void testExecute() {
		p13.execute();
		
		String result = p13.getResult().toString().substring(0, 10);
		assertEquals("5537376230", result);
	}
}
