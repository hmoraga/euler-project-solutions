package eulerProject.problems.problems1To100.problems11To20.problem11;

public enum Direction {
	LEFT_TO_RIGHT_DIAG_UP, LEFT_TO_RIGHT_DIAG_DOWN, VERTICAL, HORIZONTAL;
}
