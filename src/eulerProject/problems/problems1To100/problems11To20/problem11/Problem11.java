package eulerProject.problems.problems1To100.problems11To20.problem11;

public class Problem11 {
	private int mult;
	private int[] arr;
	private Direction direction;
	private int lengthArray;
	
	public void execute() {
		NumericArray arreglo = new NumericArray(lengthArray);
		
		mult = arreglo.getMultMaxima();
		arr = arreglo.getArrMultMax();
		direction = arreglo.getDirection();
	}

	public int getMult() {
		return mult;
	}

	public void setMult(int mult) {
		this.mult = mult;
	}

	public int[] getArr() {
		return arr;
	}

	public void setArr(int[] arr) {
		this.arr = arr;
	}

	public Direction getDirection() {
		return direction;
	}

	public void setDirection(Direction direction) {
		this.direction = direction;
	}

	public int getLengthArray() {
		return lengthArray;
	}

	public void setLengthArray(int lengthArray) {
		this.lengthArray = lengthArray;
	}
}
