package eulerProject.problems.problems1To100.problems11To20.problem11;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem11Test {
	private Problem11 p11;
	
	@Before
	public void setUp() throws Exception {
		p11 = new Problem11();
	}

	@Test
	public void testExecute() {
		p11.setLengthArray(4);
		p11.execute();
		int[] currArr = p11.getArr();
		int[] expectedArr = new int[] {89,57,36,36};
		int currMaxValue = p11.getMult();
		Direction dir = p11.getDirection();
		
		assertEquals(70600674, currMaxValue);
		assertArrayEquals(expectedArr, currArr);
		assertEquals(Direction.LEFT_TO_RIGHT_DIAG_UP, dir);
	}

}
