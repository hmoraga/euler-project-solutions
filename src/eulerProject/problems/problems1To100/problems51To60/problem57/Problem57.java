package eulerProject.problems.problems1To100.problems51To60.problem57;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Problem57 {
	public static void main(String[] args) {
		List<BigInteger> p = new ArrayList<>();
		List<BigInteger> q = new ArrayList<>();
		
		p.add(BigInteger.ONE);
		q.add(BigInteger.ONE);
		
		p.add(p.get(0).multiply(BigInteger.valueOf(2L)).add(BigInteger.ONE));
		q.add(BigInteger.valueOf(2L));
		
		int count=0;
		
		for (int i=2;i<=1000;i++) {
			BigInteger pi = BigInteger.valueOf(2L).multiply(p.get(i-1)).add(p.get(i-2));
			BigInteger qi = BigInteger.valueOf(2L).multiply(q.get(i-1)).add(q.get(i-2));
			
			p.add(pi);
			q.add(qi);
			
			if (pi.toString().length()>qi.toString().length()) {count++;}
		}
		
		System.out.println(count);
	}

}
