package eulerProject.problems.problems1To100.problems51To60.problem53;

import java.math.BigInteger;

import eulerProject.library.Math2;

public class Problem53 {

	public static void main(String[] args) {
		final long maxValue = 1000000L;
		int count=0;
		
		for (int n=1;n<=100;n++) {
			for (int r=0;r<=n;r++) {
				BigInteger num = Math2.combinatoria(n, r);

				if (num.compareTo(BigInteger.valueOf(maxValue))>0) {
					count++;
				}
			}
		}
		System.out.println("La cantidad de combinarorias mayores a "+maxValue+":"+count);
	}

}
