package eulerProject.problems.problems1To100.problems51To60.problem59;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Problem59 {

	public static void main(String[] args) throws FileNotFoundException, InterruptedException {
		String filename = "p059_cipher.txt";
		List<List<Integer>> cipheredMessagesList = getMessage(filename);
		List<String> listPossibleKeys = createListOfKeys();
		
		for (List<Integer> message : cipheredMessagesList) {
			for (int j=0;j<listPossibleKeys.size();j++) {
				String key = listPossibleKeys.get(j);
				char[] xor=new char[message.size()];
				
				for (int i=0;i<message.size();i++) {
					char c = (char)message.get(i).intValue();
					xor[i] = (char)(c^key.charAt(i % key.length()));
				}
				if (String.copyValueOf(xor).contains("the") ||
						String.copyValueOf(xor).contains("and")) {
					System.out.print("key:"+key+" "+String.copyValueOf(xor));
					System.out.println();
				}
			}
		}
	}

	private static List<String> createListOfKeys() {
		List<String> resultado = new ArrayList<>();
		
		for (int i=97;i<=122;i++) {
			String a=String.valueOf((char)i);
			for (int j=97;j<=122;j++) {
				String b=String.valueOf((char)j); 
				for (int k=97;k<=122;k++) {
					String c=String.valueOf((char)k);
					resultado.add(a.concat(b).concat(c));					
				}
			}
		}
		
		return resultado;
	}

	public static List<List<Integer>> getMessage(String filename) throws FileNotFoundException {
		List<List<Integer>> messagesList = new ArrayList<>();
		
		String userDirectory = Paths.get("")
                .toAbsolutePath()
                .toString();
		// leo los datos del archivo
		File file = new File(userDirectory+"/src/eulerProject/problems/problems1To100/problems51To60/problem59/"+filename);
		Scanner sc = new Scanner(file);
		
		while (sc.hasNextLine()) {
			List<Integer> message = new ArrayList<>();
			String line = sc.nextLine();
			line=line.trim();
			
			if (!line.isEmpty()) {
				String[] arrStr = line.split(",");

				for (int i = 0; i < arrStr.length; i++) {
					String numStr = arrStr[i];
					message.add(Integer.valueOf(numStr));
				}
			}	
			messagesList.add(message);
		}

		sc.close();
		return messagesList;
	}
	
	
}
