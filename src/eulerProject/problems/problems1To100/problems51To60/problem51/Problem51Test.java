package eulerProject.problems.problems1To100.problems51To60.problem51;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem51Test {
	private Problem51 p51;;
	
	@Before
	public void setUp() throws Exception {
		p51 = new Problem51();
	}

	@Test
	public void testExecute() {
		p51.setMin(10000000);
		p51.setMax(100000000);
		p51.execute();
		int result = p51.getResult();
		
		assertTrue(result==0);
	}

}
