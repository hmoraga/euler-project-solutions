package eulerProject.problems.problems1To100.problems51To60.problem51;

import java.util.HashMap;
import java.util.Map;

import eulerProject.library.Math2;

public class Problem51 {
	private int min;
	private int max;
	private int result;
	
	public void execute() {
		result = 0;
		
		while (min<max) {
			if (Math2.isPrime(min)) {
				Map<Integer, Integer> statistics = getStatistics(min);
				
				for (Map.Entry<Integer, Integer> entry : statistics.entrySet()) {
					if (entry.getValue().intValue() > 1) {
						result = min;
						System.out.println("found:" + result);
						min = max;
						break;
					}
				}
			} else {
				min++;
			}
		}
	}
	
	public int getMin() {
		return min;
	}
	
	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}

	private Map<Integer, Integer> getStatistics(int number) {
		Map<Integer, Integer> map = new HashMap<>();
		
		String strNum = String.valueOf(number);
		
		for (int i=0;i<strNum.length();i++) {
			String digit = strNum.substring(i, i+1);
			Integer key = Integer.valueOf(digit);
			
			if (map.containsKey(key)) {
				map.put(key, map.get(key).intValue()+1);
			} else {
				map.put(key, 1);
			}
		}
		
		return map;
	}
	
}
