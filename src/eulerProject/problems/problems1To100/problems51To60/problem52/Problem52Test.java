package eulerProject.problems.problems1To100.problems51To60.problem52;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem52Test {
	private Problem52 p52;
	
	@Before
	public void setUp() throws Exception {
		p52 = new Problem52();
	}

	@Test
	public void testExecute() {
		p52.setN(1L);
		p52.execute();
		long num = p52.getN();
		
		assertEquals(142857L, num);
	}

}
