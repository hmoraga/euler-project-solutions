package eulerProject.problems.problems1To100.problems51To60.problem52;

import eulerProject.library.Math2;

public class Problem52 {
	private long n;
	
	public void execute() {	
		while (!Math2.containsSameDigits(n, 2, 3, 4, 5, 6)) {
			n++;
		}
	}

	public long getN() {
		return n;
	}

	public void setN(long n) {
		this.n = n;
	}
}
