package eulerProject.problems.problems1To100.problems31To40.problem31;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Problem31 {
	private int count;
	private Set<List<Integer>> combinations;
	
	public void execute() {
		combinations = new HashSet<>();
		final int[] coinValues = new int[] {1, 2, 5, 10, 20, 50, 100, 200};
		final int sum = 200;
		
		// 1 pound = 100 pens -> 2 pounds ->
		// 1 coins of 2 pounds
		// 2 coins of 1 pound
		// 4 coins of 50 pens
		// 10 coins of 20 pens
		// 20 coins of 10 pens
		// 40 coins of 5 pens
		// 100 coins of 2 pens
		// 200 coins of 1 pen
		for (int p1=0;p1*coinValues[0]<=sum;p1++) {
			int p1Sum = p1*coinValues[0];
		
			for (int p2=0;p2*coinValues[1]<=sum-p1Sum;p2++) {
				int p2Sum = p2*coinValues[1];
				
				for (int p5=0;p5*coinValues[2]<=sum-p1Sum-p2Sum;p5++) {
					int p5Sum = p5*coinValues[2];
					
					for (int p10=0;p10*coinValues[3]<=sum-p1Sum-p2Sum-p5Sum;p10++) {
						int p10Sum = p10*coinValues[3];
						
						for (int p20=0;p20*coinValues[4]<=sum-p1Sum-p2Sum-p5Sum-p10Sum;p20++) {
							int p20Sum = p20*coinValues[4];
							
							for (int p50=0;p50*coinValues[5]<=sum-p1Sum-p2Sum-p5Sum-p10Sum-p20Sum;p50++) {
								int p50Sum = p50*coinValues[5];
								
								for (int p100=0;p100*coinValues[6]<=sum-p1Sum-p2Sum-p5Sum-p10Sum-p20Sum-p50Sum;p100++) {
									int p100Sum = p100*coinValues[6];
									
									for(int p200=0;p200*coinValues[7]<=sum-p1Sum-p2Sum-p5Sum-p10Sum-p20Sum-p50Sum-p100Sum;p200++) {
										int p200Sum = p200*coinValues[7];
										
										int totalSum = p1Sum+p2Sum+p5Sum+p10Sum+p20Sum+p50Sum+p100Sum+p200Sum;
										
										if (totalSum==sum) {
											int[] coins = new int[] {p1,p2,p5,p10,p20,p50,p100,p200};
											
											combinations.add(Arrays.stream(coins).boxed().collect(Collectors.toList()));
										}
									}
								}
							}
						}
					}
				}
			}
		}
		count = combinations.size();
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Set<List<Integer>> getCombinations() {
		return combinations;
	}

	public void setCombinations(Set<List<Integer>> combinations) {
		this.combinations = combinations;
	}
}
