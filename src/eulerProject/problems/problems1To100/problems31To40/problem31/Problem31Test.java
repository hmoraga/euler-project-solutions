package eulerProject.problems.problems1To100.problems31To40.problem31;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class Problem31Test {
	private Problem31 p31;

	@Before
	public void setUp() throws Exception {
		p31 = new Problem31();
	}

	@Test
	public void testExecute() {
		p31.execute();
		int count = p31.getCount();
		Integer[] expectedArr = new Integer[] {3,1,1,0,2,1,1,0};
		List<Integer> expected = Arrays.asList(expectedArr);
		
		assertEquals(73682, count);
		assertTrue(p31.getCombinations().contains(expected));
	}

}
