package eulerProject.problems.problems1To100.problems31To40.problem39;

import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import eulerProject.library.Triplet;

public class Problem39Test {
	private Problem39 p39;
	
	@Before
	public void setUp() throws Exception {
		p39 = new Problem39();
	}

	@Test
	public void testExecute() throws Exception {
		p39.setLimit(1000);
		p39.execute();
		int maxValue = p39.getMaxTriplets();
		assertEquals(16, maxValue);
	}

	@Test
	public void testGetTriangleTriplets() throws Exception {
		Set<Triplet<Integer>> set = p39.getTriangleTriplets(120);
		assertEquals(3, set.size());
	}

}
