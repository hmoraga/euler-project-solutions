package eulerProject.problems.problems1To100.problems31To40.problem39;

import java.util.HashSet;
import java.util.Set;

import eulerProject.library.Triplet;

public class Problem39 {
	private int limit;
	private int maxP;
	private int maxTriplets;
	
	public void execute() throws Exception {
		maxP = -1;
		maxTriplets = -1;

		for (int p = 1; p <= limit; p++) {
			Set<Triplet<Integer>> tripletsSet = getTriangleTriplets(p);

			if (tripletsSet.size() > maxTriplets) {
				maxP = p;
				maxTriplets = tripletsSet.size();
			}
		}
	}

	public Set<Triplet<Integer>> getTriangleTriplets(int p) throws Exception {
		Set<Triplet<Integer>> tripletsSet = new HashSet<>();

		for (int a = 1; a < p; a++) {
			for (int b = 1; b < p; b++) {
				int c = p - a - b;

				if ((c > 0) && (Math.pow(a, 2) + Math.pow(b, 2) - Math.pow(c, 2) == 0)) {
					Triplet<Integer> tri = new Triplet<>(a, b, c);
					tripletsSet.add(tri);
				}
			}
		}
		return tripletsSet;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getMaxP() {
		return maxP;
	}

	public void setMaxP(int maxP) {
		this.maxP = maxP;
	}

	public int getMaxTriplets() {
		return maxTriplets;
	}

	public void setMaxTriplets(int maxTriplets) {
		this.maxTriplets = maxTriplets;
	}
}
