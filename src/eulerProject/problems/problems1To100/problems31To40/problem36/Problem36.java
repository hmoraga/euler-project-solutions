package eulerProject.problems.problems1To100.problems31To40.problem36;

import eulerProject.library.String2;

public class Problem36 {
	private int qty;
	private int limit;
	
	public void execute() {
		qty = 0;
		
		for (int i=1;i<limit;i++) {
			String strNum = String.valueOf(i);
			String strBinary = String2.getBinary(i);
			
			if (String2.isPalindrome(strBinary) && String2.isPalindrome(strNum)) {
				qty+=i;
			}
		}
	}

	public int getQty() {
		return qty;
	}

	public void setQty(int qty) {
		this.qty = qty;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
