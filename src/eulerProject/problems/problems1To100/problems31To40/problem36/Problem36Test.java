package eulerProject.problems.problems1To100.problems31To40.problem36;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import eulerProject.library.String2;

public class Problem36Test {
	private Problem36 p36;

	@Before
	public void setUp() throws Exception {
		p36 = new Problem36();
	}

	@Test
	public void testExecute() {
		p36.setLimit(1000000);
		p36.execute();
		int qty = p36.getQty();
		assertEquals(872187, qty);
	}

	@Test
	public void testIsPalindrome() {
		String strNum = String.valueOf(585);
		String strBinary = String2.getBinary(585);
		assertTrue(String2.isPalindrome(strBinary) && String2.isPalindrome(strNum));
	}
	
}
