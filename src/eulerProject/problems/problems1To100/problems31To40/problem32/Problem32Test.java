package eulerProject.problems.problems1To100.problems31To40.problem32;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem32Test {
	private Problem32 p32;
	
	@Before
	public void setUp() throws Exception {
		p32 = new Problem32();
	}

	@Test
	public void testExecute() {
		p32.execute();
		int sum = p32.getSum();
		assertEquals(45228, sum);
	}

}
