package eulerProject.problems.problems1To100.problems31To40.problem32;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import eulerProject.library.String2;

public class Problem32 {
	private int multiplicand, multiplicator, product;
	private int sum;
	
	public void execute() { 
		List<String> permutations = String2.permutations("123456789");
		Set<PairInteger> results = new TreeSet<>();
		
		for (String stringNum : permutations) {
			// subdivide the string in 3 parts
			for (int largoi=1;largoi<7;largoi++) {
				multiplicand = Integer.parseInt(stringNum.substring(0, largoi));

				for (int largoj=1;largoj<=7;largoj++) {
					if (largoi+largoj<=8) {
						multiplicator = Integer.parseInt(stringNum.substring(largoi, largoi+largoj));	
						product = Integer.parseInt(stringNum.substring(largoi+largoj));
						
						if (multiplicand*multiplicator==product) {
							results.add(new PairInteger(multiplicand, multiplicator));
						}
					}
				}
			}
		}
		
		// the products without repeated value must be added
		Set<Integer> sumSet = new HashSet<>();
		for (PairInteger pairInteger : results) {
			sumSet.add(pairInteger.getFirst()*pairInteger.getSecond());
		}
		sum = sumSet.stream().reduce(0, (a, b) -> a+b);
	}

	public int getMultiplicand() {
		return multiplicand;
	}

	public void setMultiplicand(int multiplicand) {
		this.multiplicand = multiplicand;
	}

	public int getMultiplicator() {
		return multiplicator;
	}

	public void setMultiplicator(int multiplicator) {
		this.multiplicator = multiplicator;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}
}
