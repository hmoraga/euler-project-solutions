package eulerProject.problems.problems1To100.problems31To40.problem32;

import eulerProject.library.Pair;

public class PairInteger extends Pair<Integer,Integer> implements Comparable<PairInteger>{

	public PairInteger(Integer a, Integer b) {
		super(a, b);
		// TODO Auto-generated constructor stub
	}

	@Override
	public int compareTo(PairInteger o) {
		if ((this.getFirst().compareTo(o.getFirst())==-1) ||
				((this.getFirst().compareTo(o.getFirst())==0) && (this.getSecond().compareTo(o.getSecond())==-1)))
			return -1;
		else if ((this.getFirst().compareTo(o.getFirst())==1) ||
				((this.getFirst().compareTo(o.getFirst())==0) && (this.getSecond().compareTo(o.getSecond())==1)))
			return 1;
		else return 0;
	}
	
}
