package eulerProject.problems.problems1To100.problems31To40.problem35;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem35Test {
	private Problem35 p35;
	
	@Before
	public void setUp() throws Exception {
		p35 = new Problem35();
	}

	@Test
	public void testExample() {
		p35.setLimit(100);
		p35.execute();
		int count = p35.getCount();
		assertEquals(13, count);
	}

	@Test
	public void testExecute() {
		p35.setLimit(1000000);
		p35.execute();
		int count = p35.getCount();
		assertEquals(55, count);
	}

}
