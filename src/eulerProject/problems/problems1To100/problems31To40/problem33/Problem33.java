package eulerProject.problems.problems1To100.problems31To40.problem33;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem33 {
	private int y; //denominator
	// 4 posibilities of fractions need to be evaluated,
	// where the term to cancel erroneously is the "b" digit:
	// 
	// i)   ab/cb = a/c
	// ii)  ba/cb = a/c
	// iii) ba/bc = a/c
	// iv)  ab/bc = a/c
	// a = {1,..,9}, b = {0,..9}, c = {1,..,9}

	public void execute() {
		// there are two non-trivial cases:
		// I)  a*b-9*a*c-10*b*c = 0 (from equation ii)
		// II) 10*a*b-9*a*c-b*c = 0 (from equation iv)
		List<Integer> nums = new ArrayList<>();
		List<Integer> denoms = new ArrayList<>();
		
		for (int a=1;a<=9;a++) {
			for (int c=1;c<=9;c++) {
				for (int b=0;b<=9;b++) {
					// case 1)
					if (a*b-9*a*c-10*b*c==0 && a!=c) {
						String numerador = String.valueOf(b).concat(String.valueOf(a));
						String denominador = String.valueOf(c).concat(String.valueOf(b));
						System.out.println("ec. encontrada:" + numerador +"/"+ denominador);
						
						// found the reducted fraction
						int gcd = Math2.gcd(a, c);
						int mina = a/gcd;
						nums.add(mina);
						int minc = c/gcd;
						denoms.add(minc);
					}
					// case 2)
					if (10*a*b-9*a*c-b*c==0 && a!=c) {
						String numerador = String.valueOf(a).concat(String.valueOf(b));
						String denominador = String.valueOf(b).concat(String.valueOf(c));
						System.out.println("ec. encontrada:" + numerador +"/"+ denominador);
						
						// found the reducted fraction
						int gcd = Math2.gcd(a, c);
						int mina = a/gcd;
						nums.add(mina);
						int minc = c/gcd;
						denoms.add(minc);						
					}
				}
			}
		}
		// the examples found are multiplicated
		int x = nums.stream().reduce(1, (a,b) -> a*b);
		y = denoms.stream().reduce(1, (a,b) -> a*b);
		//then the fraction is simplified
		int gcd = Math2.gcd(x, y); 
		x = x/gcd;
		y = y/gcd;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
}

