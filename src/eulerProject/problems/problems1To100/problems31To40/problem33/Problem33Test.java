package eulerProject.problems.problems1To100.problems31To40.problem33;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem33Test {
	private Problem33 p33;
	
	@Before
	public void setUp() throws Exception {
		p33 = new Problem33();
	}

	@Test
	public void testExecute() {
		p33.execute();
		int denom = p33.getY();
		assertEquals(100, denom);
	}

}
