package eulerProject.problems.problems1To100.problems31To40.problem38;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import eulerProject.library.PairInteger;

public class Problem38 {
	private int maxNum;
	private PairInteger p;
	
	public void execute() {
		int maxN = 2;
		List<PairInteger> accomplish = new ArrayList<>();
		
		for (int num=9876;num>=9;num--) {
			int n = findNum(num);
			
			if (n>=maxN) {
				accomplish.add(new PairInteger(num, n));
			}
		}

		int maxNum = -1;
		p = null;
		
		for (int i=0;i<accomplish.size();i++) {
			int num = concatenate(accomplish.get(i));
			
			if (num > maxNum) {
				maxNum = num;
				p = accomplish.get(i);
			}
		}
		
		//System.out.println("El mayor numero se logra con ("+p.getBase()+","+p.getExp()+"):" + maxNum);
	}

	private int findNum(int num) {
		int i=1;
		List<Integer> faltantes = new LinkedList<>(Arrays.asList(1,2,3,4,5,6,7,8,9));
		int maximo=-1;
		
		do {
			String multi = String.valueOf(num*i);
			for (int j=0;j<multi.length();j++) {
				String x = multi.substring(j,j+1);
				
				if (faltantes.contains(Integer.valueOf(x))) {
					faltantes.remove(Integer.valueOf(x));
				} else return -1;
			}
			maximo = i;
			i++;
		} while (!faltantes.isEmpty());
		
		return maximo;
	}
	
	private int concatenate(PairInteger pair) {
		StringBuilder str = new StringBuilder();
		
		IntStream.rangeClosed(1, pair.getExp()).forEach(i -> str.append(String.valueOf(pair.getBase()*i)));
		
		return Integer.valueOf(str.toString());
	}

	public int getMaxNum() {
		return maxNum;
	}

	public void setMaxNum(int maxNum) {
		this.maxNum = maxNum;
	}

	public PairInteger getP() {
		return p;
	}

	public void setP(PairInteger p) {
		this.p = p;
	}
}
