package eulerProject.problems.problems1To100.problems31To40.problem38;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import eulerProject.library.PairInteger;

public class Problem38Test {
	private Problem38 p38;

	@Before
	public void setUp() throws Exception {
		p38 = new Problem38();
	}

	@Test
	public void testExecute() {
		p38.execute();
		PairInteger pair = p38.getP();
		int maxNum = p38.getMaxNum();
		assertEquals(0, maxNum);
		assertEquals(new PairInteger(1, 2), pair);
	}

}
