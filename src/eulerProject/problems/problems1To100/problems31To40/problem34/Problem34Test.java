package eulerProject.problems.problems1To100.problems31To40.problem34;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem34Test {
	private Problem34 p34;
	
	@Before
	public void setUp() throws Exception {
		p34 = new Problem34();
	}

	@Test
	public void testExecute() {
		p34.execute();
		int sum = p34.getResult();
		
		assertEquals(40730, sum);
	}

	@Test
	public void testIsCuriosNumber() {
		boolean result = p34.isCuriosNumber(145);
		assertTrue(result);
	}

}
