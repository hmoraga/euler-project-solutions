package eulerProject.problems.problems1To100.problems31To40.problem34;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem34 {
	private int result;
	
	public void execute() {
		List<Integer> numeros = new ArrayList<>();
		
		for (int i=3;i<10000000;i++) {
			if (isCuriosNumber(i)) {
				numeros.add(i);
			}
		}
		
		result = numeros.stream().reduce(0, Integer::sum);
	}
	
	public boolean isCuriosNumber(int n) {
		String strNum = String.valueOf(n);
		long localSum = 0;
		
		for (int digitIdx=0;digitIdx < strNum.length();digitIdx++) {
			int digit = Integer.parseInt(strNum.substring(digitIdx, digitIdx+1));
			
			long num = Math2.factorial(digit);
			localSum+=num;
		}
		
		return (localSum==n);
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
}
