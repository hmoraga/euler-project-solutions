package eulerProject.problems.problems1To100.problems31To40.problem37;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem37 {
	private long sum;
	private List<Long> truncatablePrimes;
	
	public void execute() {
		final int count = 11;
		int n = 10; // starting number to find primes
		truncatablePrimes = new ArrayList<>(10); 
		
		while(truncatablePrimes.size()<count) {		
			if (Math2.isTruncatablePrime(n)) {
				truncatablePrimes.add(Long.valueOf(n));
			}
			n++;
		}
		sum = truncatablePrimes.stream().reduce(0L, Long::sum);
	}

	public long getSum() {
		return sum;
	}

	public void setSum(long sum) {
		this.sum = sum;
	}

	public List<Long> getTruncatablePrimes() {
		return truncatablePrimes;
	}

	public void setTruncatablePrimes(List<Long> truncatablePrimes) {
		this.truncatablePrimes = truncatablePrimes;
	}

}
