package eulerProject.problems.problems1To100.problems31To40.problem37;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import eulerProject.library.Math2;

public class Problem37Test {
	private Problem37 p37;

	@Before
	public void setUp() throws Exception {
		p37 = new Problem37();
	}

	@Test
	public void testIsTruncatable() {
		assertTrue(Math2.isTruncatablePrime(3797));
	}
	
	@Test
	public void testExecute() {
		p37.execute();
		long sum = p37.getSum();
		assertEquals(748317, sum);
	}

}
