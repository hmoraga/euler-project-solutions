package eulerProject.problems.problems1To100.problems31To40.problem40;

public class Problem40 {
	private int limit;
	private int result;
	
	public void execute() {
		StringBuilder strBuilder = new StringBuilder(10*limit+3);

		for (int i=1;i<=limit;i++) {
			strBuilder.append(i);
		}
		
		int pot10 = 1;
		result = 1;
		
		do {
			int n = Integer.parseInt(strBuilder.substring(pot10-1, pot10));
			result = result*n;
			pot10*=10;
		} while (pot10<=limit);
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
}
