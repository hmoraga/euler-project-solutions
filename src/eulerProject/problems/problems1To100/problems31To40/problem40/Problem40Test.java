package eulerProject.problems.problems1To100.problems31To40.problem40;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem40Test {
	private Problem40 p40;
	
	@Before
	public void setUp() throws Exception {
		p40 = new Problem40();
		p40.setLimit(1000000);
	}

	@Test
	public void testExecute() {
		p40.execute();
		int result = p40.getResult();
		assertEquals(210, result);
	}

}
