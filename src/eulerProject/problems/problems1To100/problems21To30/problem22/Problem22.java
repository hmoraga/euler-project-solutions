package eulerProject.problems.problems1To100.problems21To30.problem22;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Problem22 {
	private long score;
	private String filename;
	
	public void execute() throws FileNotFoundException {
		String userDirectory = Paths.get("").toAbsolutePath().toString();
		//String filename = "p022_names.txt";
		File file = new File(userDirectory + "/src/eulerProject/problems/problems1To100/problems21To30/problem22//" + filename);
		Scanner sc = new Scanner(file);
		score=0L;

		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			line = line.trim();
			String[] names = line.split(",");
			List<String> nombres = Arrays.stream(names).map(x -> x.replace("\"","")).collect(Collectors.toList());
			Collections.sort(nombres);
			
			for (int idx = 0;idx<nombres.size();idx++) {
				String name = nombres.get(idx);
				long puntaje = calculateScore(name, idx+1);
				score+=puntaje;
			}
		}
		sc.close();
	}
	
	private long calculateScore(String name, int place) {
		long localSum = 0L;
				
		for (int idx=0;idx<name.length();idx++) {
			int val = name.charAt(idx)-'A'+1;
			localSum+=val;
		}
		
		return localSum*place;
	}

	public long getScore() {
		return score;
	}

	public void setScore(long score) {
		this.score = score;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}
}
