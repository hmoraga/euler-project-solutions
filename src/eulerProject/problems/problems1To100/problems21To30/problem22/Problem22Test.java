package eulerProject.problems.problems1To100.problems21To30.problem22;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class Problem22Test {
	private Problem22 p22;
	
	@Before
	public void setUp() throws Exception {
		p22 = new Problem22();
	}

	@Test
	public void testExecute() throws FileNotFoundException {
		p22.setFilename("p022_names.txt");
		p22.execute();
		long score = p22.getScore();
		assertEquals(871198282L, score);
	}

	@Test
	public void testExample() throws FileNotFoundException {
		p22.setFilename("example.txt");
		p22.execute();
		long score = p22.getScore();
		assertEquals(53L, score);
	}

}
