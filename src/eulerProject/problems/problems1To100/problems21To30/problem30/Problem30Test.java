package eulerProject.problems.problems1To100.problems21To30.problem30;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem30Test {
	private Problem30 p30;
	
	@Before
	public void setUp() throws Exception {
		p30 = new Problem30();
	}

	@Test
	public void testExecute() {
		p30.setExp(5);
		p30.execute();
		long num = p30.getSum();
		assertEquals(443839L, num);
	}

	@Test
	public void testNthPowerOf4() {
		long num = p30.nthPower(4, 4);
		assertEquals(256L, num);
	}

	@Test
	public void testNthPowerOf5() {
		long num = p30.nthPower(7, 5);
		assertEquals(16807L, num);
	}
	
}
