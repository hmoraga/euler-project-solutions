package eulerProject.problems.problems1To100.problems21To30.problem30;

import java.util.ArrayList;
import java.util.List;

public class Problem30 {
	private long sum;
	private int exp;
	
	public void execute() {
		List<Long> numeros = new ArrayList<>();
		
		for (long i=10L;i<999999;i++) {
			long valor = nthPower(i, exp);
			
			if (valor==i) {
				numeros.add(i);
			}
		}
		sum = numeros.stream().reduce(0L, Long::sum);
	}

	public long nthPower(long i, int exp) {
		String aux = String.valueOf(i);
		String[] s = new String[aux.length()];
		long sum = 0;
		
		for (int j = 0; j < aux.length(); j++) {
			s[j]=aux.substring(j, j+1);
			sum+=(long)Math.pow(Integer.parseInt(s[j]), exp);
		}
		
		return sum;
	}

	public long getSum() {
		return sum;
	}

	public void setSum(long sum) {
		this.sum = sum;
	}

	public int getExp() {
		return exp;
	}

	public void setExp(int exp) {
		this.exp = exp;
	}
}
