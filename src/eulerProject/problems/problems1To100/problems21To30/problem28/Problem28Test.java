package eulerProject.problems.problems1To100.problems21To30.problem28;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem28Test {
	private Problem28 p28;
	
	@Before
	public void setUp() throws Exception {
		p28 = new Problem28();
	}

	@Test
	public void testExecute() {
		p28.setLimit(1001);
		p28.execute();
		long sum = p28.getSum();
		assertEquals(669171001L, sum);
	}

	@Test
	public void testExample() {
		p28.setLimit(5);
		p28.execute();
		long sum = p28.getSum();
		assertEquals(101L, sum);
	}

}
