package eulerProject.problems.problems1To100.problems21To30.problem28;

public class Problem28 {
	private int limit;
	private long sum;
	private int[][] arr;
	
	private enum DIRECTION {
		LEFT,DOWN,RIGHT,UP
	}
	
	public void execute() {
		fillArray(limit);
		sum = addDiagonals()-arr[(int)(limit/2)][(int)(limit/2)]; // because the middle is sum twice
	}

	private long addDiagonals() {
		long localSum = 0L;
		
		for (int i=0;i<arr.length;i++) {
			localSum+=arr[i][i];
		}

		for (int i=0;i<arr.length;i++) {
			localSum+=arr[i][arr.length-1-i];
		}
		
		return localSum;
	}

	private void fillArray(int length) {
		int n = (int)Math.pow(length, 2);
		
		arr = new int[length][length];
		
		DIRECTION idxDir = DIRECTION.LEFT;
		// initial positions
		int maxCol = arr.length - 1;
		int minCol = 0;
		int maxRow = arr.length - 1;
		int minRow = 0;

		int row = 0;
		int col = arr.length - 1;

		while (n > 0) {
			switch (idxDir) {
			case LEFT:
				while (col>=minCol) {
					if (arr[row][col]==0) {
						arr[row][col]=n;
					}
					
					n--;
					
					if (col>minCol) {col--;} else {break;}
				}
				
				if (n==0) {break;}
				n++;  // small adjustment when we change direction
				minRow++;
				idxDir = DIRECTION.DOWN;
				break;
			case DOWN:
				while (row<=maxRow) {
					if (arr[row][col]==0) {
						arr[row][col] = n;						
					}
					
					n--;

					if (row<maxRow) {row++;} else {break;}
				}
				if (n==0) {break;}
				n++;  // small adjustment when we change direction
				minCol++;
				idxDir = DIRECTION.RIGHT;
				break;
			case RIGHT:
				while (col<=maxCol) {
					if (arr[row][col]==0) {
						arr[row][col] = n;						
					}
					
					n--;

					if (col<maxCol) {col++;} else {break;}
				}
				if (n==0) {break;}
				n++;  // small adjustment when we change direction
				maxRow--;
				idxDir = DIRECTION.UP;
				break;
			case UP:
				while (row>=minRow) {
					if (arr[row][col]==0) {
						arr[row][col] = n;						
					}
					
					n--;

					if (row>minRow) {row--;} else {break;}
				}
				if (n==0) {break;}
				n++;  // small adjustment when we change direction
				maxCol--;
				idxDir = DIRECTION.LEFT;
				break;
			default:
				break;
			}
		}
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getSum() {
		return sum;
	}

	public void setSum(long sum) {
		this.sum = sum;
	}
}
