package eulerProject.problems.problems1To100.problems21To30.problem26;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem26Test {
	private Problem26 p26;
	
	@Before
	public void setUp() throws Exception {
		p26 = new Problem26();
	}

	@Test
	public void testExecute() {
		p26.execute();
		int denom = p26.getMaxD();
		int periodLength = p26.getMaxPeriod();
		assertEquals(983, denom);
		assertEquals(982, periodLength);
	}

}
