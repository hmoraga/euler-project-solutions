package eulerProject.problems.problems1To100.problems21To30.problem26;

import eulerProject.library.String2;

public class Problem26 {
	private int maxD;
	private int maxPeriod;
	
	public void execute() {
		/* based on the answer of
		 * https://math.stackexchange.com/questions/377683/length-of-period-of-decimal-expansion-of-a-fraction#:~:text=The%20length%20of%20the%20period%20is%20given%20by%20the%20multiplicative,where%20q%20is%20your%20quotient.
		 *
		 * the idea is to find the first number n (integer) that accomplish the following:
		 *   10^n % d = 1 
		 */	
		final int numerator = 1;
		maxD = 0;
		maxPeriod = 0;
		
		for (int d=2;d<1000;d++) {
			int denominator = d;
			
			String[] result = String2.fractionToDecimal(numerator, denominator);
			//result[0] = el decimal en el formato del ejemplo (ver arch README.md)
			//result[1] = el periodo
			if (result[1].length()>maxPeriod) {
				maxPeriod = result[1].length();
				maxD = d;
			}
			
			//showFraction(numerator, denominator, result);
		}
		//System.out.println("El período máximo se da con d="+maxD+" con largo " + maxPeriod);
	}

	/*private void showFraction(final int numerator, int denominator, String[] result) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder
			.append(String.valueOf(numerator))
			.append("/")
			.append(String.valueOf(denominator))
			.append(" = ");
		
		if (result[1].isEmpty()) {
			strBuilder.append(BigInteger.ONE.divide(BigInteger.valueOf(denominator)).toString());
		} else {
			strBuilder.append(String.valueOf(numerator/denominator)).append(".").append(result[0]);
		}
		System.out.println(strBuilder.toString());
	}*/

	public int getMaxD() {
		return maxD;
	}

	public void setMaxD(int maxD) {
		this.maxD = maxD;
	}

	public int getMaxPeriod() {
		return maxPeriod;
	}

	public void setMaxPeriod(int maxPeriod) {
		this.maxPeriod = maxPeriod;
	}
}
