package eulerProject.problems.problems1To100.problems21To30.problem24;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class Problem24Test {
	private Problem24 p24;
	
	@Before
	public void setUp() throws Exception {
		p24 = new Problem24();
	}

	@Test
	public void testExecute() {
		p24.setInitialString("0123456789");
		p24.setLimit(1000000);
		p24.execute();
		String perm = p24.getPermutation();
		assertEquals("2783915460", perm);
	}

	@Test
	public void testExample() {
		p24.setInitialString("012");
		p24.setLimit(6);
		p24.execute();
		String perm = p24.getPermutation();
		assertEquals("210", perm);
	}

}
