package eulerProject.problems.problems1To100.problems21To30.problem24;

import java.util.Collections;
import java.util.List;

import eulerProject.library.String2;

public class Problem24 {
	private String permutation;
	private String initialString;
	private int limit;
	
	public void execute() {
		List<String> res = String2.permutations(initialString);
		Collections.sort(res);
		
		permutation = res.get(limit-1);
	}

	public String getPermutation() {
		return permutation;
	}

	public void setPermutation(String permutation) {
		this.permutation = permutation;
	}

	public String getInitialString() {
		return initialString;
	}

	public void setInitialString(String initialString) {
		this.initialString = initialString;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
