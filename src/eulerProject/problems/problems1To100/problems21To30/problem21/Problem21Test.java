package eulerProject.problems.problems1To100.problems21To30.problem21;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class Problem21Test {
	private Problem21 p21;
	
	@Before
	public void setUp() throws Exception {
		p21 = new Problem21();
	}

	@Test
	public void testExecute() {
		p21.setLimit(10000);
		p21.execute();
		int sum = p21.getSum();
		assertEquals(31626, sum);
	}

	@Test
	public void testExample() {
		p21.setLimit(300);
		p21.execute();
		int sum = p21.getSum();
		Set<Integer> results = p21.getResults();
		Set<Integer> expected = new HashSet<>();
		expected.add(220);
		expected.add(284);
		
		assertEquals(504, sum);
		assertEquals(expected, results);
	}
	
}
