package eulerProject.problems.problems1To100.problems21To30.problem21;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import eulerProject.library.Math2;

public class Problem21 {
	private int limit;
	private int sum;
	private Set<Integer> results;
	
	public Problem21() {
		results = new HashSet<>();		
	}
	
	public void execute() {
		for (int a=1;a<limit;a++) {
			List<Integer> na = Math2.properDivisors(a);
			
			int da = na.stream().reduce(0, Integer::sum);
			
			List<Integer> nb = Math2.properDivisors(da);
			
			int db = nb.stream().reduce(0, Integer::sum);
			
			if (a==db && a!=da) {
				results.add(a);
				results.add(da);
			}
		}
		sum = results.stream().reduce(0, Integer::sum);
	}

	public int getSum() {
		return sum;
	}

	public void setSum(int sum) {
		this.sum = sum;
	}

	public Set<Integer> getResults() {
		return results;
	}

	public void setResults(Set<Integer> results) {
		this.results = results;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
