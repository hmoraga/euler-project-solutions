package eulerProject.problems.problems1To100.problems21To30.problem27;

import eulerProject.library.Pair;

public class PairInteger extends Pair<Integer, Integer> implements Comparable<PairInteger>{

	public PairInteger(Integer a, Integer b) {
		super(a, b);
	}

	public int compareTo(PairInteger other) {
		Integer p0 = this.a*this.b;
		Integer p1 = other.getFirst()*other.getSecond();
		
		if (p0 > p1) {return 1;}
		else if (p0 < p1) {return -1;} 
		else {return 0;}
	}

}
