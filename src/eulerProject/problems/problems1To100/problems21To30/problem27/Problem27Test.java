package eulerProject.problems.problems1To100.problems21To30.problem27;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem27Test {
	private Problem27 p27;
	
	@Before
	public void setUp() throws Exception {
		p27 = new Problem27();
	}

	@Test
	public void testExecute() {
		p27.setLimit(1000);
		p27.execute();
		int maxA = p27.getMaxA();
		int maxB = p27.getMaxB();
		int maxPrimes = p27.getMaxConsecutivePrimes();
		assertEquals(-59231, maxA*maxB);
		assertEquals(maxPrimes, 71);
	}

	@Test
	public void testGetConsecutivePrimesExample1() {
		int num = p27.getConsecutivePrimes(1, 41);
		assertEquals(40, num);
	}

	@Test
	public void testGetConsecutivePrimesExample2() {
		int num = p27.getConsecutivePrimes(-79, 1601);
		assertEquals(80, num);
	}

}
