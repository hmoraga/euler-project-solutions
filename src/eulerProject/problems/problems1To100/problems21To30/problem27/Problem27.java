package eulerProject.problems.problems1To100.problems21To30.problem27;

import java.util.HashSet;
import java.util.Set;

import eulerProject.library.Math2;

public class Problem27 {
	private int maxA;
	private int maxB;
	private int maxConsecutivePrimes;
	private int limit;

	public void execute() {
		/* The idea that n^2+a*n+b generates prime numbers means that
		 * a combination of (a,b) must be search that the polynomium
		 * can't be factorized with real numbers.
		 * Is needed a^2-4*b &lt 0
		 */

		// first search the values that accomplished the  
		Set<PairInteger> set = new HashSet<>();

		for (int a = -limit+1; a < limit; a++) {
			for (int b = -limit; b <= limit; b++) {
				if (Math.pow(a, 2) < 4 * b) {
					set.add(new PairInteger(a, b));
				}
			}
		}

		// for each pair we check how many consecutive primes can be generated
		maxA = Integer.MIN_VALUE;
		maxB = Integer.MIN_VALUE;
		maxConsecutivePrimes = 0;

		for (PairInteger pair : set) {
			int total = getConsecutivePrimes(pair.getBase(), pair.getExp());

			if (total > maxConsecutivePrimes) {
				maxA = pair.getBase();
				maxB = pair.getExp();
				maxConsecutivePrimes = total;
			}
		}
	}

	public int getConsecutivePrimes(int a, int b) {
		int cont = 0;
		int n = 0;

		while (Math2.isPrime((int) (Math.pow(n, 2) + a * n + b))) {
			cont++;
			n++;
		}

		return cont;
	}

	public int getMaxA() {
		return maxA;
	}

	public void setMaxA(int maxA) {
		this.maxA = maxA;
	}

	public int getMaxB() {
		return maxB;
	}

	public void setMaxB(int maxB) {
		this.maxB = maxB;
	}

	public int getMaxConsecutivePrimes() {
		return maxConsecutivePrimes;
	}

	public void setMaxConsecutivePrimes(int maxConsecutivePrimes) {
		this.maxConsecutivePrimes = maxConsecutivePrimes;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
