package eulerProject.problems.problems1To100.problems21To30.problem29;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Problem29 {
	private int totalTerms;
	private int aLimit;
	private int bLimit;
	
	public void execute() {
		Set<BigInteger> resultsSet = new HashSet<>();
		Map<Integer, Integer> repeatedValues = new HashMap<>();
		
		for (int a=2;a<=aLimit;a++) {
			
			for (int b=2;b<=bLimit;b++) {
				BigInteger powValue = new BigInteger(String.valueOf(a)).pow(b);
				
				if (!resultsSet.add(powValue)) {
					repeatedValues.put(a, b);
				}
			}
		}
		
		//System.out.println("El total de terminos es:" + resultsSet.size());

		/*System.out.print("Terminos repetidos:");
		for (Map.Entry<Integer, Integer> entry : repeatedValues.entrySet()) {
			Integer key = entry.getKey();
			Integer val = entry.getValue();
			
			System.out.print("("+ key + ", " + val+") ");
		}
		System.out.println();*/
		totalTerms = resultsSet.size();
	}

	public int getTotalTerms() {
		return totalTerms;
	}

	public void setTotalTerms(int totalTerms) {
		this.totalTerms = totalTerms;
	}

	public int getaLimit() {
		return aLimit;
	}

	public void setaLimit(int aLimit) {
		this.aLimit = aLimit;
	}

	public int getbLimit() {
		return bLimit;
	}

	public void setbLimit(int bLimit) {
		this.bLimit = bLimit;
	}
}
