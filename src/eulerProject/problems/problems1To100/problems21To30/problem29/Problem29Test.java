package eulerProject.problems.problems1To100.problems21To30.problem29;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem29Test {
	private Problem29 p29;
	
	@Before
	public void setUp() throws Exception {
		p29 = new Problem29();
	}

	@Test
	public void testExecute() {
		p29.setaLimit(100);
		p29.setbLimit(100);
		p29.execute();
		int num = p29.getTotalTerms();
		assertEquals(9183, num);
	}

	@Test
	public void testExample() {
		p29.setaLimit(5);
		p29.setbLimit(5);
		p29.execute();
		int num = p29.getTotalTerms();
		assertEquals(15, num);
	}
}
