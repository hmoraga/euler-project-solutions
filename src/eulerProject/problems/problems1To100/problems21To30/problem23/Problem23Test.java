package eulerProject.problems.problems1To100.problems21To30.problem23;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem23Test {
	private Problem23 p23;
	
	@Before
	public void setUp() throws Exception {
		p23 = new Problem23();
	}

	@Test
	public void testExecute() {
		p23.execute();
		long sum = p23.getSum();
		assertEquals(6L, sum);
	}

}
