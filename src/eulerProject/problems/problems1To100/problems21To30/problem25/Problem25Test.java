package eulerProject.problems.problems1To100.problems21To30.problem25;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem25Test {
	private Problem25 p25;
	
	@Before
	public void setUp() throws Exception {
		p25 = new Problem25();
	}

	@Test
	public void testExecute() {
		int limitLength = 1000;
		
		p25.setLimit(limitLength);
		p25.execute();
		
		String value = p25.getValue();
		int num = p25.getN();
		
		assertEquals(4782, num);
		assertTrue(value.length()>=limitLength);
	}

	@Test
	public void testExample() {
		int limitLength = 3;
		
		p25.setLimit(limitLength);
		p25.execute();
		
		String value = p25.getValue();
		int num = p25.getN();
		
		assertEquals(12, num);
		assertTrue(value.length()>=limitLength);
	}
	
}
