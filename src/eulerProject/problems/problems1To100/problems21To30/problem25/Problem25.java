package eulerProject.problems.problems1To100.problems21To30.problem25;

import eulerProject.library.Math2;

public class Problem25 {
	private int limit;
	private int n;
	private String value;
	
	public void execute() {
		n = 0;
			
		do {
			n++;
			value = Math2.fibonacci(n).toString();
			//System.out.println("el factorial "+n+"! tiene "+result.length() +" dígitos");
		} while (value.length()<limit);	
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
