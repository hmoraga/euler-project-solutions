package eulerProject.problems.problems1To100.problems01To10.problem10;

import java.util.ArrayList;
import java.util.List;

public class Problem10 {
	private int limit;
	private long suma;
	
	public void exec() {
		List<Integer> primeNumbers = new ArrayList<>();
		
		for (int i=2;i<limit;i++) {
			if (esPrimo(i)) {
				primeNumbers.add(i);
			}
		}
		
		suma = primeNumbers.stream()
				.map(x -> Long.valueOf(x.longValue()))
				.reduce(0L, Long::sum);
	}

	public static boolean esPrimo(int num) {
		for (int i=2;i<=(int)Math.sqrt(num);i++) {
			if (num%i==0) {
				return false;
			}
		}
		return true;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getSuma() {
		return suma;
	}

	public void setSuma(long suma) {
		this.suma = suma;
	}
}
