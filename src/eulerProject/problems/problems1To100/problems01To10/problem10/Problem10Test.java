package eulerProject.problems.problems1To100.problems01To10.problem10;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem10Test {
	private Problem10 p10;
	
	@Before
	public void setUp() throws Exception {
		p10 = new Problem10();
	}

	@Test
	public void testExample() {
		p10.setLimit(10);
		p10.exec();
		long current = p10.getSuma();
		
		assertEquals(17L, current);
	}

	@Test
	public void testExec() {
		p10.setLimit(2000000);
		p10.exec();
		long current = p10.getSuma();
		
		assertEquals(142913828922L, current);
	}

	@Test
	public void testEsPrimoTrue() {
		boolean result = Problem10.esPrimo(17);
		assertTrue(result);
	}

	@Test
	public void testEsPrimoFalse() {
		boolean result = Problem10.esPrimo(111);
		assertFalse(result);
	}
	
}
