package eulerProject.problems.problems1To100.problems01To10.problem9;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem9Test {
	private Problem9 p9;
	
	@Before
	public void setUp() throws Exception {
		p9 = new Problem9();
	}

	@Test
	public void testExec() {
		p9.exec();
		int a = p9.getA();
		int b = p9.getB();
		int c = p9.getC();
		int prod = p9.getProd();
		
		assertEquals(200, a);
		assertEquals(375, b);
		assertEquals(425, c);
		assertEquals(31875000, prod);
	}

}
