package eulerProject.problems.problems1To100.problems01To10.problem9;

public class Problem9 {
	private int a, b, c;
	private int prod;
	
	public int getA() {
		return a;
	}

	public void setA(int a) {
		this.a = a;
	}

	public int getB() {
		return b;
	}

	public void setB(int b) {
		this.b = b;
	}

	public int getC() {
		return c;
	}

	public void setC(int c) {
		this.c = c;
	}

	public int getProd() {
		return prod;
	}

	public void setProd(int prod) {
		this.prod = prod;
	}

	public void exec() {
		double cx;
		
		for (int ax=1;ax<1000;ax++) {
			for (int bx=1;bx<1000;bx++) {
				if (ax<bx) {
					cx = Math.sqrt(Math.pow(ax, 2)+Math.pow(bx, 2));
					
					if ((cx<1000) && (ax+bx+cx==1000)) {
						a = ax;
						b = bx;
						c = (int)cx;
						prod = a*b*c;
						break;
					}				
				}
			}
		}		
	}
}
