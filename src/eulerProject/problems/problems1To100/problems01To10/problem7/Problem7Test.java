package eulerProject.problems.problems1To100.problems01To10.problem7;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem7Test {
	private Problem7 p7;
	
	@Before
	public void setUp() throws Exception {
		p7 = new Problem7();
	}

	@Test
	public void testExecute() {
		p7.setLimit(10001);
		p7.execute();
		long prime = p7.getPrimeNum();
		
		assertEquals(104743L, prime);
	}

	@Test
	public void testExample() {
		p7.setLimit(6);
		p7.execute();
		long prime = p7.getPrimeNum();
		
		assertEquals(13L, prime);
	}
}
