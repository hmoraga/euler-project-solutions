package eulerProject.problems.problems1To100.problems01To10.problem7;

import eulerProject.library.Math2;

public class Problem7 {
	private int contPrimos;
	private int limit;
	private long primeNum;

	public void execute() {
		long n = 1;

		do {
			n++;

			if (Math2.isPrime(n)) {
				contPrimos++;
			}
		} while (contPrimos < limit);
		primeNum = n;
	}

	public int getContPrimos() {
		return contPrimos;
	}

	public void setContPrimos(int contPrimos) {
		this.contPrimos = contPrimos;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getPrimeNum() {
		return primeNum;
	}

	public void setPrimeNum(long primeNum) {
		this.primeNum = primeNum;
	}	
}
