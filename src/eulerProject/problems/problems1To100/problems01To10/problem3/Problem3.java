package eulerProject.problems.problems1To100.problems01To10.problem3;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem3 {
	private static List<Long> example = new ArrayList<>();
	private static List<Long> solution = new ArrayList<>();
	
	public static void execute() {
		long n=13195L;
		example = Math2.getPrimeFactors(n);
		//System.out.println("Los factores primos de "+n+" son:" + ejemplo);
		long n1 = 600851475143L;
		solution = Math2.getPrimeFactors(n1);
		//System.out.println("Los factores primos de "+n1+" son:" + solucion);
	}

	public static List<Long> getExample() {
		return example;
	}

	public static void setExample(List<Long> example) {
		Problem3.example = example;
	}

	public static List<Long> getSolution() {
		return solution;
	}

	public static void setSolution(List<Long> solution) {
		Problem3.solution = solution;
	}
}
