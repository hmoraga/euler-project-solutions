package eulerProject.problems.problems1To100.problems01To10.problem3;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class Problem3Test {
	@Before
	public void init() {
		Problem3.execute();
	}

	@Test
	public void exampleTest() {
		List<Long> example = Problem3.getExample();
		Long[] expected = new Long[] {5L, 7L, 13L, 29L};
		Long[] current = example.toArray(new Long[0]);
		assertArrayEquals(expected, current);
	}

	@Test
	public void calculate() {
		List<Long> result = Problem3.getSolution();
		Long maxElem = result.stream().max(Long::compareTo).get();
		assertEquals(6857L, maxElem.longValue());
	}
	
}
