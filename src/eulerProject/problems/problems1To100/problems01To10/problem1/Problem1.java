package eulerProject.problems.problems1To100.problems01To10.problem1;

import java.util.Optional;
import java.util.stream.IntStream;

public class Problem1 {
	private static int sum = 0;

	private static boolean isMultOf3(int n) {
		return (n%3==0);
	}
	
	private static boolean isMultOf5(int n) {
		return (n%5==0);
	}
	
	public static int calculate() {
		Optional<Integer> suma = IntStream.rangeClosed(1, 999)
				.boxed()
				.filter(i -> isMultOf3(i) || isMultOf5(i))
				.reduce((a,b) -> a+b);
		
		if (suma.isPresent()) {
			sum=suma.get();
		}
		
		return sum;
	}	
}
