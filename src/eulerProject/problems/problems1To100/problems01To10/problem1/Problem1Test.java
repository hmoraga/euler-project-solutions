package eulerProject.problems.problems1To100.problems01To10.problem1;

import static org.junit.Assert.*;

import org.junit.Test;

public class Problem1Test {

	@Test
	public void testCalculate() {
		int result = Problem1.calculate();
		
		assertEquals(233168, result);
	}

}
