package eulerProject.problems.problems1To100.problems01To10.problem8;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem8Test {
	Problem8 p8;
	
	@Before
	public void setUp() throws Exception {
		p8 = new Problem8();
	}

	@Test
	public void testExample() {
		p8.setDigits(4);
		p8.exec();
		long currentNum = p8.getMaxProducto();
		int[] currentArr = p8.getArrNumsMax();
		int[] expectedArr = new int[] {9,9,8,9};

		assertEquals(5832L, currentNum);
		assertArrayEquals(expectedArr, currentArr);
	}

	@Test
	public void testExecute() {
		p8.setDigits(13);
		p8.exec();
		long currentNum = p8.getMaxProducto();
		int[] currentArr = p8.getArrNumsMax();
		int[] expectedArr = new int[] {5,5,7,6,6,8,9,6,6,4,8,9,5};

		assertEquals(23514624000L, currentNum);
		assertArrayEquals(expectedArr, currentArr);
	}

}
