package eulerProject.problems.problems1To100.problems01To10.problem5;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem5Test {
	private Problem5 p5;

	@Before
	public void setUp() throws Exception {
		p5 = new Problem5();
	}

	@Test
	public void testExecute() {
		p5.setNum(20);
		long actual = p5.execute();
		assertEquals(232792560L, actual);
	}

	@Test
	public void testExample() {
		p5.setNum(10);
		long actual = p5.execute();
		assertEquals(2520L, actual);
	}
	
}
