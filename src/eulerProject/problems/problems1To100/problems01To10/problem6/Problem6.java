package eulerProject.problems.problems1To100.problems01To10.problem6;

public class Problem6 {
	private int n;
	private int result;
	
	public void execute() {
		// suma de los primeros 100 numeros naturales
		int a = sumNaturals(n);
		// suma de los cuadrados de los primeros 100 numeros naturales
		int b = sumSquaresOfNaturals(n);
		// lo que se pide
		result = (int)(Math.pow(a, 2)-b);
	}
	
	public int sumNaturals(int num) {
		return num*(num+1)/2;
	}
	
	public int sumSquaresOfNaturals(int num) {
		return num*(num+1)*(2*num+1)/6;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}
}
