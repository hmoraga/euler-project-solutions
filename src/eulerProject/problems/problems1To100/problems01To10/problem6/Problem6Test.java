package eulerProject.problems.problems1To100.problems01To10.problem6;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem6Test {
	private Problem6 p6;
	
	@Before
	public void setUp() throws Exception {
		p6 = new Problem6();
	}

	@Test
	public void testExecute() {
		p6.setN(20);
		p6.execute();
		int result = p6.getResult();
		assertEquals(41230, result);
	}

	@Test
	public void testExample() {
		p6.setN(10);
		p6.execute();
		int result = p6.getResult();
		assertEquals(2640, result);
	}

	@Test
	public void testSumNaturals() {
		int n = p6.sumNaturals(10);
		assertEquals(55, n);
	}

	@Test
	public void testSumSquaresOfNaturals() {
		int n = p6.sumSquaresOfNaturals(10);
		assertEquals(385, n);
	}

}
