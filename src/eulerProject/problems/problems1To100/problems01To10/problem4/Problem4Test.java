package eulerProject.problems.problems1To100.problems01To10.problem4;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem4Test {
	Problem4 p4;
	@Before
	public void init() {
		p4 = new Problem4();
	}
	
	@Test
	public void testExample() {
		p4.execute(2);
		
		int palindrome = p4.getPalindrome();
		int n1 = p4.getN1();
		int n2 = p4.getN2();
		
		assertEquals(91, Math.min(n1, n2));
		assertEquals(99, Math.max(n1, n2));
		assertEquals(9009, palindrome);
	}

	@Test
	public void testSolution() {
		p4.execute(3);
		
		int palindrome = p4.getPalindrome();
		int n1 = p4.getN1();
		int n2 = p4.getN2();
		
		assertEquals(913, Math.min(n1, n2));
		assertEquals(993, Math.max(n1, n2));
		assertEquals(906609, palindrome);
	}
	
}
