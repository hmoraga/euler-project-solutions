package eulerProject.problems.problems1To100.problems01To10.problem4;

import eulerProject.library.String2;

public class Problem4 {
	private int n1;
	private int n2;
	private int palindrome;
	
	public void execute(int nDigits) {
		int maxN1=0;
		int maxN2=0;
		int calc = 0;
		
		for (int i = (int)Math.pow(10, nDigits)-1; i >= (int)Math.pow(10, nDigits-1)+1; i--) {
			for (int j = i - 1; j >= (int)Math.pow(10, nDigits-1); j--) {
				int prod = i*j;
				if (String2.isPalindrome(String.valueOf(prod))) {
					if (prod > calc) {
						calc = prod;
						maxN1 = i;
						maxN2 = j;
					}
				}
			}
		}
		
		n1 = maxN1;
		n2 = maxN2;
		palindrome = calc;
	}

	public int getN1() {
		return n1;
	}

	public void setN1(int n1) {
		this.n1 = n1;
	}

	public int getN2() {
		return n2;
	}

	public void setN2(int n2) {
		this.n2 = n2;
	}

	public int getPalindrome() {
		return palindrome;
	}

	public void setPalindrome(int palindrome) {
		this.palindrome = palindrome;
	}
}