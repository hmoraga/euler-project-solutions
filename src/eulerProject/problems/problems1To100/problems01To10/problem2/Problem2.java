package eulerProject.problems.problems1To100.problems01To10.problem2;

import java.util.HashMap;
import java.util.Map;

public class Problem2 {
	private static double PHI1 = (1+Math.sqrt(5))/2;
	private static double PHI2 = (1-Math.sqrt(5))/2;
	private static Map<Integer, Long> memoizationCache = new HashMap<>();
	
	public static long calculate() {
		long val1 = 1;
		long val2 = 1;
		memoizationCache.put(0, val2);
		memoizationCache.put(1, val1);
		int n = 2;
		long result = val2 + val1;
		memoizationCache.put(n, result);
		
		while (result < 4000000L) {
			n = n + 1;
			val2 = (memoizationCache.containsKey(n-2))?memoizationCache.get(n-2):fibonacci(n-2);
			val1 = (memoizationCache.containsKey(n-1))?memoizationCache.get(n-1):fibonacci(n-1);
			result = val1 + val2;
			memoizationCache.put(n, result);
		};
		
		return memoizationCache.entrySet().stream()
			.map(es -> es.getValue())
			.filter(num -> num % 2 == 0)
			.reduce(0L, Long::sum);
	}

	/**
	 * Fibonacci using generating function
	 * @param n
	 * @return fib(n)
	 */
	public static long fibonacci(int n) {
		return (long)((Math.pow(PHI1, n)-Math.pow(PHI2, n))/Math.sqrt(5));
	}
}
