package eulerProject.problems.problems1To100.problems01To10.problem2;

import static org.junit.Assert.*;

import org.junit.Test;

public class Problem2Test {

	@Test
	public void testCalculate() {
		long x = Problem2.calculate();
		assertEquals(4613732L, x);
	}

	@Test
	public void testFibonacci() {
		int n=8;
		
		long res = Problem2.fibonacci(n);
		assertEquals(21L, res);
	}

}
