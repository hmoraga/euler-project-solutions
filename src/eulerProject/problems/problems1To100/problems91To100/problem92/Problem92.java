package eulerProject.problems.problems1To100.problems91To100.problem92;

public class Problem92 {
	public static void main(String[] args) {
		int cantNum89 = 0;

		for (int i=1; i<10000000;i++) {
			String strNum = String.valueOf(i);
			
			do {
				int sum=0;
				for (int j=0;j<strNum.length();j++) {
					int ch = strNum.charAt(j)-48;
					sum+=Math.pow(ch, 2);
				}
				strNum=String.valueOf(sum);
			} while (!strNum.equals("1") && !strNum.equals("89"));
			
			if (strNum.equals("89")) {
				cantNum89++;
			}
		}
		
		System.out.println("Cantidad de numeros que terminan en 89:"+cantNum89);
	}
}
