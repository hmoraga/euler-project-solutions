package eulerProject.problems.problems1To100.problems41To50.problem49;

import java.util.List;
import java.util.stream.Collectors;

import eulerProject.library.Math2;
import eulerProject.library.String2;

public class Problem49 {
	private int x,y,z;
	private String seq;
	
	public void execute() {
		int[][] distancia;

		for (int num=1000;num<10000;num++) {
			List<Integer> permutaciones = String2.permutations(String.valueOf(num))
					.stream().map(Integer::valueOf)
					.filter(x -> x>1000)
					.filter(Math2::isPrime)
					.sorted()
					.collect(Collectors.toList());
			
			if (permutaciones.size()>=2) {
				// lleno una matriz triangular superior con las distancias del
				// elemento fila con el resto de las permutaciones.
				// busco si existen 2 que tengan la misma distancia entre si
				distancia= new int[permutaciones.size()][permutaciones.size()];
				
				for (int fila=0;fila<permutaciones.size();fila++) {
					for (int col=0;col<permutaciones.size();col++) {
						distancia[fila][col]=permutaciones.get(col)-permutaciones.get(fila);
					}
				}
				
				// hago la busqueda, de valores iguales
				boolean found = false;
				for (int fila=0;!found && fila<permutaciones.size()-1;fila++) {
					for (int col=fila+1;!found && col<permutaciones.size();col++) {
						// tengo la distancia entre el elemento en la
						// posicion fila y el pos col
						int distF1 = distancia[fila][col];
						// busco en la fila con valor col si existe
						// un elemento con el mismo valor
						for (int col2=0;col2<permutaciones.size();col2++) {
							if (distancia[col][col2]==distF1) {
								x = permutaciones.get(fila);
								y = permutaciones.get(col);
								z = permutaciones.get(col2);
								found = true;
							}
						}
					}
				}
				
				seq = getSequence();
			}	
		}
	}

	private String getSequence() {
		String seq = "";
		
		if (x<y) {
			seq = seq.concat(String.valueOf(x));
			if (y<z) {
				seq = seq.concat(String.valueOf(y)).concat(String.valueOf(z));
			} else {
				seq = seq.concat(String.valueOf(z)).concat(String.valueOf(y));
			}
		} else {
			seq = seq.concat(String.valueOf(y));
			if (x<z) {
				seq = seq.concat(String.valueOf(x)).concat(String.valueOf(z));
			} else {
				seq = seq.concat(String.valueOf(z)).concat(String.valueOf(x));
			}
		}
		
		return seq;
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}

	public int getZ() {
		return z;
	}

	public void setZ(int z) {
		this.z = z;
	}

	public String getSeq() {
		return seq;
	}

	public void setSeq(String seq) {
		this.seq = seq;
	}
	
}