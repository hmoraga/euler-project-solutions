package eulerProject.problems.problems1To100.problems41To50.problem49;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem49Test {
	Problem49 p49;
	
	@Before
	public void setUp() throws Exception {
		p49 = new Problem49();
	}

	@Test
	public void testExecute() {
		p49.execute();
		String seq = p49.getSeq();
		assertEquals("296962999629", seq);
	}

}
