package eulerProject.problems.problems1To100.problems41To50.problem41;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.IntStream;

import eulerProject.library.Math2;
import eulerProject.library.String2;

public class Problem41 {
	private long maxPrime;
	private static final String digits = "1234567890";

	public void execute() {
		List<Long> primos = new ArrayList<>();
		
		IntStream
			.range(1, digits.length())
			.boxed()
			.map(i -> digits.substring(0, i))
			.map(String2::permutations)
			.flatMap(List::stream)
			.map(Long::parseLong)
			.filter(n -> Math2.isPrime(n))
			.forEach(n -> {
				primos.add(n);
			});
		
		
		Collections.sort(primos);
		maxPrime = primos.get(primos.size()-1);
	}

	public long getMaxPrime() {
		return maxPrime;
	}

	public void setMaxPrime(long maxPrime) {
		this.maxPrime = maxPrime;
	}
}
