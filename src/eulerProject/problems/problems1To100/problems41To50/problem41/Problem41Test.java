package eulerProject.problems.problems1To100.problems41To50.problem41;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem41Test {
	private Problem41 p41;
	
	@Before
	public void setUp() throws Exception {
		p41 = new Problem41();
	}

	@Test
	public void testExecute() {
		p41.execute();
		long maxPrime = p41.getMaxPrime();
		assertEquals(7652413, maxPrime);
	}

}
