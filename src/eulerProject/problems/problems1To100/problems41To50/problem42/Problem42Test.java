package eulerProject.problems.problems1To100.problems41To50.problem42;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;

import org.junit.Before;
import org.junit.Test;

public class Problem42Test {
	private Problem42 p42;
	
	@Before
	public void setUp() throws Exception {
		p42 = new Problem42();
	}

	@Test
	public void testExecute() throws FileNotFoundException {
		p42.setFilename("p042_words.txt");
		p42.execute();
		long count = p42.getCount();
		assertEquals(162L, count);
	}

}
