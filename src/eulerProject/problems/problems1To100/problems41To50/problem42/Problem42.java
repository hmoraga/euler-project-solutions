package eulerProject.problems.problems1To100.problems41To50.problem42;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import eulerProject.library.Math2;
import eulerProject.library.String2;

public class Problem42 {
	private String filename;
	private long count;
	
	public void execute() throws FileNotFoundException {
		String userDirectory = Paths.get("").toAbsolutePath().toString();
		List<String> wordList = new ArrayList<>();
		count = 0;
		File file = new File(userDirectory + "/src/eulerProject/problems/problems1To100/problems41To50/problem42/" + filename);

		try (Scanner sc = new Scanner(file)) {
			while (sc.hasNextLine()) {
				String line = sc.nextLine();
				line = line.trim();

				if (!line.isEmpty()) {
					String[] words = line.split(",");

					wordList.addAll(Arrays.stream(words)
							.map(word -> word.replace("\"", ""))
							.collect(Collectors.toList()));
				}
			}
		}
		
		//System.out.println("cantidad de palabras:" + wordList.size());
		count = IntStream.range(0, wordList.size())
		.boxed()
		.map(i -> wordList.get(i))
		.map(String2::getNumberOfWord)
		.filter(Math2::isTriangular)
		.count();
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public long getCount() {
		return count;
	}

	public void setCount(long count) {
		this.count = count;
	}
}
