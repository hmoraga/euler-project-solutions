package eulerProject.problems.problems1To100.problems41To50.problem47;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eulerProject.library.Math2;
import eulerProject.library.PairLong;

public class Problem47 {
	private long limit;
	private long i;
	Map<Long, List<PairLong>> primeFactorsList;
	
	public void execute() {
		primeFactorsList = new HashMap<>();
		
		for (i = 2L; i < limit; i++) {
			if (!primeFactorsList.containsKey(i)) {
				// prime factors are calculated
				List<PairLong> primeFactors = Math2.primeFactors(i);
				// the list of primeFactors is added to the map when they have 4 or more factors
				if (primeFactors.size()>=4) {
					primeFactorsList.put(i, primeFactors);
				}
			}
			
			/* then is checked if the factors are 4 consecutive values
			 * and if each of them have 4 or more prime factors, the 
			 * cycle ends */
			boolean existenFactoresPrimosConsecutivos =
					primeFactorsList.containsKey(i) &&
					primeFactorsList.containsKey(i-1) &&
					primeFactorsList.containsKey(i-2) &&
					primeFactorsList.containsKey(i-3);
			if (existenFactoresPrimosConsecutivos) {
				boolean tienenCuatroFactores = (primeFactorsList.get(i).size()==4) &&
						(primeFactorsList.get(i-1).size()==4) &&
						(primeFactorsList.get(i-2).size()==4) &&
						(primeFactorsList.get(i-3).size()==4);
						
				if (tienenCuatroFactores) {
					/*System.out.println("Los cuatro enteros consecutivos con 4 factores primos:"+(i-3)+","+(i-2)+","+(i-1)+","+i);
					System.out.println(mostrarFactores(i-3, primeFactorsList.get(i-3)));
					System.out.println(mostrarFactores(i-2, primeFactorsList.get(i-2)));
					System.out.println(mostrarFactores(i-1, primeFactorsList.get(i-1)));
					System.out.println(mostrarFactores(i, primeFactorsList.get(i))); */
					break;
				}
			}
		}
	}

	public String showFactors(long l, List<PairLong> factorsList) {
		StringBuilder strBldr = new StringBuilder(String.valueOf(l)).append(":");
		
		for (int i=0;i<factorsList.size();i++) {
			if (i==0) {
				strBldr = strBldr.append("[");
				strBldr = (factorsList.get(i).getExp()>1)?strBldr.append(String.valueOf(factorsList.get(i).getBase())).append("^").append(String.valueOf(factorsList.get(i).getExp())):
					strBldr.append(String.valueOf(factorsList.get(i).getBase()));
 			} else if (i==factorsList.size()-1) {
				strBldr = (factorsList.get(i).getExp()>1)?strBldr.append("x").append(String.valueOf(factorsList.get(i).getBase())).append("^").append(String.valueOf(factorsList.get(i).getExp())):
					strBldr.append("x").append(String.valueOf(factorsList.get(i).getBase()));
				strBldr = strBldr.append("]");	
			} else {
				strBldr = (factorsList.get(i).getExp()>1)?strBldr.append("x").append(String.valueOf(factorsList.get(i).getBase())).append("^").append(String.valueOf(factorsList.get(i).getExp())):
					strBldr.append("x").append(String.valueOf(factorsList.get(i).getBase()));
			}
		}
		
		return strBldr.toString();
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public long getI() {
		return i;
	}

	public void setI(long i) {
		this.i = i;
	}

	public Map<Long, List<PairLong>> getPrimeFactorsList() {
		return primeFactorsList;
	}

	public void setPrimeFactorsList(Map<Long, List<PairLong>> primeFactorsList) {
		this.primeFactorsList = primeFactorsList;
	}
}
