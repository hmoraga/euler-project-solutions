package eulerProject.problems.problems1To100.problems41To50.problem47;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem47Test {
	private Problem47 p47;
	
	@Before
	public void setUp() throws Exception {
		p47 = new Problem47();
	}

	@Test
	public void testExecute() {
		p47.setLimit(1000000L);
		p47.execute();
		
		long num = p47.getI();
		
		assertEquals(1, num-3L);
		assertEquals(2, num-2L);
		assertEquals(3, num-1L);
		assertEquals(4, num);
		assertEquals("", p47.showFactors(num-3L, p47.getPrimeFactorsList().get(num-3L)));
		assertEquals("", p47.showFactors(num-2L, p47.getPrimeFactorsList().get(num-2L)));
		assertEquals("", p47.showFactors(num-1L, p47.getPrimeFactorsList().get(num-1L)));
		assertEquals("", p47.showFactors(num, p47.getPrimeFactorsList().get(num)));
		
	}

}
