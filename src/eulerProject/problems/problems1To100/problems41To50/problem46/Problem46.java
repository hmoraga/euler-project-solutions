package eulerProject.problems.problems1To100.problems41To50.problem46;

import java.util.List;

import eulerProject.library.Math2;

public class Problem46 {
	private int k;
	private int limit;
	
	public void execute() {
		/* numbers involved
		 * i) the odd composite number can be represented by 2*k+1 (and can't be
		 *    a prime number).
		 * ii) the prime number can be represented by 2*m+1, m>0
		 * iii) the double of a square of a number can be represented by 2*p^2
		 * 
		 * the pair of equations to be accomplished are:
		 * 
		 *  2*k+1 = 2*m+1+2*p^2, m>0, p>0
		 *    k-m = p^2, with k > m > 0
		 */
		
		for (k=4;k<limit;k++) {
			if (Math2.isPrime(2*k+1)) {continue;}
			long p2=0;
			
			boolean result = false;
			List<Integer> primos = Math2.primesList(2*k+1);
			//System.out.println("n:"+(2*k+1)+" "+primos.toString());
			
			for (Integer m : primos) {
				p2 = (2*k+1-m)/2;
				
				if (Math2.isPerfectSquareWithOptimization(p2)) {
					//System.out.println(""+(2*k+1)+"="+m+"+2*"+p2);
					result=true;
					break;
				}
			}
			
			if (!result) {
				break;
			}
		}
	}

	public int getK() {
		return k;
	}

	public void setK(int k) {
		this.k = k;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}
}
