package eulerProject.problems.problems1To100.problems41To50.problem46;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem46Test {
	private Problem46 p46;
	
	@Before
	public void setUp() throws Exception {
		p46 = new Problem46();
	}

	@Test
	public void testExecute() {
		p46.setLimit(100000);
		p46.execute();
		int num = 2*p46.getK()+1;
		
		assertEquals(5777, num);
	}

}
