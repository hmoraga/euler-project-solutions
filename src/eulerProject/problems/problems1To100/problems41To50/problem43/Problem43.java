package eulerProject.problems.problems1To100.problems41To50.problem43;

import eulerProject.library.Math2;
import eulerProject.library.String2;

public class Problem43 {
	private long sum;
	
	public void execute() {
		sum = String2.permutations("1234567890").stream()
				.filter(s -> isCuriousNumber(s))
				.map(Long::valueOf)
				.reduce(0L, Long::sum);
	}

	public long getSum() {
		return sum;
	}

	public void setSum(long sum) {
		this.sum = sum;
	}
	
	public boolean isCuriousNumber(String numStr) {
		boolean divisibleBy2 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(1, 4)), 2);
		boolean divisibleBy3 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(2, 5)), 3);
		boolean divisibleBy5 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(3, 6)), 5);
		boolean divisibleBy7 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(4, 7)), 7);
		boolean divisibleBy11 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(5, 8)), 11);
		boolean divisibleBy13 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(6, 9)), 13);
		boolean divisibleBy17 = Math2.isDivisibleBy(Integer.valueOf(numStr.substring(7, 10)), 17);
	
		return (divisibleBy2 && divisibleBy3 && divisibleBy5 && divisibleBy7
				&& divisibleBy11 && divisibleBy13 && divisibleBy17);
	}
}
