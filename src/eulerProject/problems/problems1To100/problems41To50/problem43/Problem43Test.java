package eulerProject.problems.problems1To100.problems41To50.problem43;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem43Test {
	private Problem43 p43;

	@Before
	public void setUp() throws Exception {
		p43 = new Problem43();
	}

	@Test
	public void testExecute() {
		p43.execute();
		long sum = p43.getSum();
		assertEquals(16695334890L, sum);
	}

	@Test
	public void testIsCuriousNumber() {
		boolean result = p43.isCuriousNumber("1406357289");
		assertTrue(result);
	}

}
