package eulerProject.problems.problems1To100.problems41To50.problem45;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem45 {
	private long limit;
	private long result;
	// for each function generator of triangular, pentagonal and hexagonal sequences, 
	// they need to generate the same value. So the three equations:
	//
	// i)   a^2+a-2k   = 0 (triangular)
	// ii)  3*b^2-b-2k = 0 (pentagonal)
	// iii) 2*c^2-c-k  = 0 (hexagonal)
	//
	// so we can iterate, from k=40755 to obtain the next number that
	// is triangular, pentagonal and also hexagonal

	public void execute() {
		long firstNum = 40755L;
		
		List<Long> resultado = new ArrayList<>();
		resultado.add(firstNum);
		
		long k = firstNum+1;
		
		do {
			// if we see the square root of each equation we
			// need to check if 8*k+1 and 24*k+1 are perfect
			// squares
			if (Math2.isPerfectSquareWithOptimization(8*k+1) &&
					Math2.isPerfectSquareWithOptimization(24*k+1)) {
				// the other checks are:
				if (((-1+(long)Math.sqrt(8*k+1))%2==0) &&
						((1+(long)Math.sqrt(24*k+1))%6==0) &&
						((-1+(long)Math.sqrt(8*k+1))%4==0)) {
					resultado.add(k);
					break;
				}
			}
			k++;
		} while (resultado.size()<2);
		
		/*for (long n=1L;n<limit;n++) {
			triangulares.add(Math2.triangular(n));
			pentagonales.add(Math2.pentagonal(n));
			hexagonales.add(Math2.hexagonal(n));
		}
		
		for (Long num : triangulares) {
			if (pentagonales.contains(num) && hexagonales.contains(num)) {
				resultado.add(num);
			}
		}
		
		Collections.sort(resultado);*/		
		//System.out.println(resultado.get(1));
		result = resultado.get(1);
	}

	public long getLimit() {
		return limit;
	}

	public void setLimit(long limit) {
		this.limit = limit;
	}

	public long getResult() {
		return result;
	}

	public void setResult(long result) {
		this.result = result;
	}
}
