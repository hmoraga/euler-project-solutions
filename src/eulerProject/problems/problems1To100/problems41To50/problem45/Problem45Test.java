package eulerProject.problems.problems1To100.problems41To50.problem45;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem45Test {
	private Problem45 p45;
	
	@Before
	public void setUp() throws Exception {
		p45 = new Problem45();
	}

	@Test
	public void testExecute() {
		p45.setLimit(1000000L);
		p45.execute();
		long result = p45.getResult();
		assertEquals(567645L, result);
	}

}
