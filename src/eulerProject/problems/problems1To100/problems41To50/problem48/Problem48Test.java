package eulerProject.problems.problems1To100.problems41To50.problem48;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem48Test {
	private Problem48 p48;
	
	@Before
	public void setUp() throws Exception {
		p48 = new Problem48();
	}

	@Test
	public void testExample() {
		p48.setN(10);
		p48.execute();
		String strNum = p48.getStr();
		assertEquals("10405071317", strNum);
	}

	@Test
	public void testExecute() {
		p48.setN(1000);
		p48.execute();
		String digits = p48.getDigits();
		assertEquals("9110846700", digits);
	}

}
