package eulerProject.problems.problems1To100.problems41To50.problem48;

import java.math.BigInteger;

public class Problem48 {
	private String digits;
	private String str;
	private int n;

	public void execute() {
		BigInteger suma = BigInteger.valueOf(0L);
		
		for (int i=1;i<=n;i++) {
			suma=suma.add(new BigInteger(String.valueOf(i)).pow(i));
		}
		
		str = suma.toString();
		digits = str.substring(str.length()-10);
	}

	public String getDigits() {
		return digits;
	}

	public void setDigits(String digits) {
		this.digits = digits;
	}

	public int getN() {
		return n;
	}

	public void setN(int n) {
		this.n = n;
	}

	public String getStr() {
		return str;
	}

	public void setStr(String str) {
		this.str = str;
	}
	
}
