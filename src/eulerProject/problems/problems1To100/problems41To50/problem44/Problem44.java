package eulerProject.problems.problems1To100.problems41To50.problem44;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

import eulerProject.library.Math2;
import eulerProject.library.PairLong;

public class Problem44 {
	private static Logger logger = Logger.getLogger(Problem44.class.getName());
	private List<PairLong> listPentagonalPairs;
	private int limit;
	private long minD;
	
	public void execute() {
		PairLong pentagonalPairs;
		
		List<Long> pentagonalList = Arrays.stream(LongStream.range(1, limit).map(Math2::pentagonal).toArray()).boxed().collect(Collectors.toList());
		minD = Long.MAX_VALUE;
		
		for (int k=0;k<pentagonalList.size()-1;k++) {
			long pk = pentagonalList.get(k);
			for (int j=1;j<pentagonalList.size();j++) {
				long pj = pentagonalList.get(j);
				long sum = pj+pk;
				long diff = Math.abs(pk-pj);
				
				if (pentagonalList.contains(sum) &&
						pentagonalList.contains(diff)
						&& (minD > diff)) {
					logger.log(Level.INFO, "pj:" + pj + " pk:" + pk + " |pj-pk|:" + diff);
					pentagonalPairs = new PairLong(pj, pk);
				}
			}
		}		
	}

	public List<PairLong> getListPentagonalPairs() {
		return listPentagonalPairs;
	}

	public void setListPentagonalPairs(List<PairLong> listPentagonalPairs) {
		this.listPentagonalPairs = listPentagonalPairs;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getMinD() {
		return minD;
	}

	public void setMinD(long minD) {
		this.minD = minD;
	}
}
