package eulerProject.problems.problems1To100.problems41To50.problem44;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem44Test {
	private Problem44 p44;
	@Before
	public void setUp() throws Exception {
		p44 = new Problem44();
	}

	@Test
	public void testExecute() {
		p44.execute();
		long d = p44.getMinD();
		assertEquals(9223372036854775807L, d);
	}

}
