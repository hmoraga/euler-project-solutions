package eulerProject.problems.problems1To100.problems41To50.problem44;

import eulerProject.library.Pair;

public class PairLong extends Pair<Long,Long> implements Comparable<PairLong>{

	public PairLong(Long a, Long b) {
		super(a, b);
	}
	
	@Override
	public int compareTo(PairLong o) {
		Long p0 = this.a*this.b;
		Long p1 = o.getFirst()*o.getSecond();
		
		if (p0 > p1) {return 1;}
		else if (p0 < p1) {return -1;} 
		else {return 0;}
	}

}
