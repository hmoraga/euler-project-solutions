package eulerProject.problems.problems1To100.problems41To50.problem50;

import java.util.ArrayList;
import java.util.List;

import eulerProject.library.Math2;

public class Problem50 {
	private int limit;
	private long maxPrime;
	private int maxFirstTerm;
	private int maxCantTerms;
	
	public void execute() {
		List<Integer> primes = new ArrayList<>();

		for (int i = 2; i < 10000; i++) {
			if (Math2.isPrime(i)) {
				primes.add(i);
			}
		}

		maxFirstTerm = 0;
		maxCantTerms = 0;
		maxPrime = 0L;

		// genero un map con la suma de consecutivos como clave y como valor un par de
		// numeros
		// el primero es el primer termino y el segundo la cantidad de primos
		// consecutivos
		for (int i = 0; i < primes.size() - 1; i++) {
			int firstTerm = primes.get(i);

			for (int j = i + 2; j < primes.size(); j++) {
				List<Integer> subList = primes.subList(i + 1, j);
				long suma = firstTerm + subList.stream().map(Long::valueOf).reduce(0L, Long::sum);

				if (Math2.isPrime(suma) && suma < limit && (subList.size() > maxCantTerms)) {
					maxPrime = suma;
					maxCantTerms = subList.size();
					maxFirstTerm = firstTerm;
				}
			}
		}

		//System.out.println("primo:" + maxPrime + " " + maxFirstTerm + "+... " + (maxCantTerms + 1) + " términos");
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public long getMaxPrime() {
		return maxPrime;
	}

	public void setMaxPrime(long maxPrime) {
		this.maxPrime = maxPrime;
	}

	public int getMaxFirstTerm() {
		return maxFirstTerm;
	}

	public void setMaxFirstTerm(int maxFirstTerm) {
		this.maxFirstTerm = maxFirstTerm;
	}

	public int getMaxCantTerms() {
		return maxCantTerms;
	}

	public void setMaxCantTerms(int maxCantTerms) {
		this.maxCantTerms = maxCantTerms;
	}
}
