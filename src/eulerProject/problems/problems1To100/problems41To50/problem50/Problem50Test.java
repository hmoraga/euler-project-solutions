package eulerProject.problems.problems1To100.problems41To50.problem50;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem50Test {
	Problem50 p50;
	
	@Before
	public void setUp() throws Exception {
		p50 = new Problem50();
	}

	@Test
	public void testExecute() {
		p50.setLimit(1000000);
		p50.execute();
		assertEquals(997651L,p50.getMaxPrime());
		assertEquals(7, p50.getMaxFirstTerm());
		assertEquals(543, p50.getMaxCantTerms()+1);
	}

	@Test
	public void testExample() {
		p50.setLimit(100);
		p50.execute();
		long prime = p50.getMaxPrime();
		assertEquals(41L, prime);
		assertEquals(2, p50.getMaxFirstTerm());
		assertEquals(6, p50.getMaxCantTerms()+1);
	}
}
