package eulerProject.problems.problems1To100.problems61To70.problem63;

import java.math.BigInteger;

public class Problem63 {

	public static void main(String[] args) {
		int cont=0;
		
		for (int base=1;base<=100;base++) {
			for (int exp=1;exp<=100;exp++) {
				BigInteger num = BigInteger.valueOf(base).pow(exp);				
				if (num.toString().length()==exp) {
					System.out.println(base+"^"+exp+"="+num.toString());
					cont++;
				}
			}
		}
		System.out.println("cantidad:"+cont);
	}

}
