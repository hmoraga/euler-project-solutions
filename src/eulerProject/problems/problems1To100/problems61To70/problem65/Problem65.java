package eulerProject.problems.problems1To100.problems61To70.problem65;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

public class Problem65 {
	public static void main(String[] args) {
		List<BigInteger> p = new ArrayList<>();
		List<BigInteger> q = new ArrayList<>();

		p.add(BigInteger.valueOf(2L));
		q.add(BigInteger.ONE);
		
		List<BigInteger> sequence = new ArrayList<>();
		sequence.add(BigInteger.ONE);
		sequence.add(BigInteger.valueOf(2L));
		sequence.add(BigInteger.ONE);
		int k=1;
		List<BigInteger> ai = new ArrayList<>(100);
		ai.add(BigInteger.valueOf(2L)); //a0
		
		for (int i=1;i<100;i++) {
			BigInteger seq = sequence.get((i-1)%3);
			
			if (seq.compareTo(BigInteger.valueOf(2L))==0) {
				seq=seq.multiply(BigInteger.valueOf(k));
				k++;
			}
			System.out.print(seq.toString()+" ");
			ai.add(seq);
		}
		System.out.println();
		
		for (int i=1;i<100;i++) {
			BigInteger pi = ai.get(i).multiply(p.get(i-1)).add((i>1)?p.get(i-2):BigInteger.ONE);
			BigInteger qi = ai.get(i).multiply(q.get(i-1)).add((i>1)?q.get(i-2):BigInteger.ZERO);
			
			p.add(pi);
			q.add(qi);
		}
		
		String bigNum = p.get(p.size()-1).toString();
		int sum = 0;
		
		for (int j=0;j<bigNum.length();j++) {
			sum+=Integer.valueOf(bigNum.substring(j, j+1));
		}
		
		System.out.println(sum);
	}
}