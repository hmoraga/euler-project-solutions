package eulerProject.problems.problems801To900.problems801To810.problem808;

import java.math.BigInteger;
import java.util.Set;
import java.util.TreeSet;

import eulerProject.library.Math2;
import eulerProject.library.String2;

public class Problem808 {

	public static void main(String[] args) {
		Set<Long> listado = new TreeSet<>();
		long i=2L;
		
		do {
			if (Math2.isPrime(i)) {
				long sqr = i*i;
				boolean notPalindrome = String2.isPalindrome(String.valueOf(sqr));
				long sqrInv = Long.parseLong(String2.reverse(String.valueOf(sqr)));
				double x = Math.sqrt(sqrInv);
				long xt = (long)x*(long)x;
					
				if ((xt == sqrInv) && !notPalindrome && Math2.isPrime((long)x)) {
					System.out.println("Valores encontrados: "+sqr+" "+sqrInv);
					listado.add(sqr);
					listado.add(sqrInv);
					System.out.println("Tamaño del listado:"+listado.size());
				}
			}
			i++;
		} while (listado.size()<50);
		
		System.out.println("listado:"+listado.toString());

		BigInteger suma = listado.stream().map(BigInteger::valueOf).reduce(BigInteger.ZERO, (subtotal, element) -> subtotal.add(element));
		System.out.println("La suma es:"+suma.toString());
	}
}
