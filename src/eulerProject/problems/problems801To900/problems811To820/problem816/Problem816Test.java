package eulerProject.problems.problems801To900.problems811To820.problem816;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class Problem816Test {
	private Problem816 p816;
	
	@Before
	public void setUp() throws Exception {
		p816 = new Problem816();
	}

	@Test
	public void testExecute() {
		p816.setNumPoints(2000000);		
		p816.execute();
		double dist = p816.getMinDist();
		assertEquals(60.207972894, dist, 1E-9);
	}

	@Test
	public void testExample() {
		p816.setNumPoints(14);		
		p816.execute();
		double dist = p816.getMinDist();
		assertEquals(546446.466846479, dist, 1E-9);
	}
	
}
