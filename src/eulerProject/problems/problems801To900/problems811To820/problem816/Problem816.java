package eulerProject.problems.problems801To900.problems811To820.problem816;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Logger;

import eulerProject.library.PairLong;

public class Problem816 {
	private int numPoints;
	private double minDist;
	private final long s0 = 290797L;
	private static Long divisor = 50515093L;
	private static Logger LOG = Logger.getLogger("Problem816");
	private List<Long> memoization;
	private List<PairLong> listPoints; 
	
	public void execute() {
		memoization = new ArrayList<>(2*numPoints);
		listPoints = new ArrayList<PairLong>(numPoints);
		
		for (int i=0; i<numPoints; i++) {
			//calculate the s_n data values
			calculateS(2*i);
			calculateS(2*i+1);
			// obtain the list of Points
			listPoints.add(new PairLong(memoization.get(2*i), memoization.get(2*i+1)));
		}
		
		// obtain the minimal distance using divide and conquer strategy
		minDist = getMinDist(listPoints);
	}
	
	private double getDist(PairLong p0, PairLong p1) {
		return Math.sqrt(Math.pow(p0.getFirst()-p1.getFirst(), 2.0)+Math.pow(p0.getSecond()-p1.getSecond(), 2.0));
	}

	private double getMinDist(List<PairLong> listPoints2) {
		if (listPoints2==null) {
			throw new IllegalArgumentException();
		} else {
			int n = listPoints2.size();
			
			if (n==1) {
				return Double.MAX_VALUE;
			} else if (n==2) {
				PairLong p0 = listPoints2.get(0);
				PairLong p1 = listPoints2.get(1);
			
				return getDist(p0, p1);
			} else if (n==3) {
				double minDist = Double.MAX_VALUE;
			
				for (int i=0;i<listPoints2.size()-1;i++) {
					PairLong p0 = listPoints2.get(i);
					for (int j=i+1;j<listPoints2.size();j++) {
						PairLong p1 = listPoints2.get(j);
						double d = getDist(p0, p1);
					
						if (d < minDist) {
							minDist = d;
						}
					}
				}
				return minDist;
			} else {
				// sort points by first coordinate
				Collections.sort(listPoints2, new Comparator<PairLong>() {
					@Override
					public int compare(PairLong o1, PairLong o2) {
						if ((o1.getFirst() < o2.getFirst()) || 
							((o1.getFirst()==o2.getFirst()) && (o1.getSecond() < o2.getSecond()))) {
							return -1;
						} else if ((o1.getFirst() > o2.getFirst()) ||
							((o1.getFirst()==o2.getFirst()) && (o1.getSecond() > o2.getSecond()))) {
							return 1;
						} else {
							return 0;
						}
					}
				});
				// divide points in 2 sublists
				int mid = n/2;
				List<PairLong> left = listPoints2.subList(0, mid);
				List<PairLong> right = listPoints2.subList(mid, listPoints2.size());
				
				double minLeft = getMinDist(left);
				double minRight = getMinDist(right);
				
				// get the min of two sublists
				return Math.min(minLeft, minRight);
			}
		}
	}

	private void calculateS(int n) {
		if (n==0) {
			memoization.add(s0);
		} else {
			memoization.add(Math.floorMod(memoization.get(n-1)*memoization.get(n-1), divisor));
		}
	}
	
	public PairLong getPoint(int n) {
		return new PairLong(memoization.get(2*n), memoization.get(2*n+1));
	}
		
	public int getNumPoints() {
		return numPoints;
	}

	public void setNumPoints(int numPoints) {
		this.numPoints = numPoints;
	}

	public double getMinDist() {
		return minDist;
	}

	public void setMinDist(double minDist) {
		this.minDist = minDist;
	}
}
