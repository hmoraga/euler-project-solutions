package eulerProject.problems.problems801To900.problems811To820.problem816;

import eulerProject.library.Pair;

public class PairInteger extends Pair<Integer, Integer> implements Comparable<PairInteger>{

	public PairInteger(Integer a, Integer b) {
		super(a, b);
	}

	@Override
	public int compareTo(PairInteger o) {
		if ((this.getFirst() < o.getFirst()) 
				|| (this.getFirst().equals(o.getFirst()) && this.getSecond() < o.getSecond()))
			return -1;
		else if ((this.getFirst() > o.getFirst())
				|| (this.getFirst().equals(o.getFirst()) && this.getSecond() > o.getSecond()))
			return 1;
		else
			return 0;
	}

}
