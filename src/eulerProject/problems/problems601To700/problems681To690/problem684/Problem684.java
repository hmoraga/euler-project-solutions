package eulerProject.problems.problems601To700.problems681To690.problem684;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import eulerProject.library.Math2;

public class Problem684 {
	public static void main(String[] args) {
		final int n = 90;
		final BigInteger mod = BigInteger.valueOf(1000000007L);
		
		System.out.println("Calculando fibonacci");
		Map<BigInteger, BigInteger> fibonacci = Math2.fibonacciWithCache(n);
		System.out.println("fin fibonacci");
		for (Map.Entry<BigInteger, BigInteger> entry : fibonacci.entrySet()) {
			BigInteger key = entry.getKey();
			BigInteger val = entry.getValue();
			System.out.println("f("+key.toString()+")="+val.toString());
		}

		System.out.println("Calculando el numero mas pequeño cuya suma de digitos sea fibonacci de 2 a 90 en mod " + mod.toString());
		Map<BigInteger, BigInteger> smallestNumber = new HashMap<>();
		
		for (Map.Entry<BigInteger, BigInteger> entry : fibonacci.entrySet()) {
			long t0 = System.currentTimeMillis();
			BigInteger key = entry.getKey();
			BigInteger val = entry.getValue();
			BigInteger s = Math2.findSmallestNumberWithGivenDigitsSum(val, mod);
			smallestNumber.put(val, s);
			System.out.println("s(f("+key+"))="+s.toString());
			long t1 = System.currentTimeMillis();
			System.out.println("t1-t0=" + (t1-t0));
		}
		System.out.println("Fin del cálculo de los s minuscula");
		
		System.out.println("Calculando la sumatoria de los s minuscula");
		Map<BigInteger, BigInteger> sumSmallestNumber = new HashMap<>(); //cache

		for (Map.Entry<BigInteger, BigInteger> entry : fibonacci.entrySet()) {
			long t0 = System.currentTimeMillis();
			BigInteger key = entry.getKey();
			BigInteger val = entry.getValue();
			
			/*if (sumSmallestNumber.isEmpty()) {
				BigInteger S = Math2.sumOfFindSmallestNumberWithGivenDigitsSum(val, mod);
				sumSmallestNumber.put(val, S);
			} else {
				BigInteger maxKey = sumSmallestNumber.keySet().stream()
						.filter(k -> k.compareTo(val) < 0 )
						.max(BigInteger::compareTo)
						.orElseThrow(NoSuchElementException::new);
				BigInteger sum = sumSmallestNumber.get(maxKey);
				
				for (BigInteger k=maxKey.add(BigInteger.ONE);k.compareTo(val)<=0;k=k.add(BigInteger.ONE)) {
					sum=(sum.add(Math2.findSmallestNumberWithGivenDigitsSum(k, mod))).mod(mod);
				}
				sumSmallestNumber.put(val, sum);
			}
			System.out.println("S(f("+key+"))="+sumSmallestNumber.get(val));
			long t1 = System.currentTimeMillis();
			System.out.println("t1-t0=" + (t1-t0));*/
		}
		System.out.println("Fin del cálculo de la sumatoria de los s minuscula");		
	}
}
