package eulerProject.problems.problems601To700.problems681To690.problem686;

import eulerProject.library.Math2;

public class Problem686 {
	public static void main(String[] args) {
		int lead = 123;
		int times = 678910;
		int expo = Math2.findPower(lead, times);
		
		System.out.println("The number is:" + expo);
	}
}
