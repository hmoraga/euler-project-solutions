package eulerProject.problems.problems601To700.problems691To700.problem700;

import java.math.BigInteger;
import java.util.Collection;
import java.util.Map;
import java.util.TreeMap;

public class Problem700 {

	public static void main(String[] args) {
		final BigInteger n = BigInteger.valueOf(4503599627370517L);
		final BigInteger a = BigInteger.valueOf(1504170715041707L);
		final BigInteger aInv = a.modInverse(n);
		Map<BigInteger, BigInteger> parciales = new TreeMap<>();

		// primer elemento
		parciales.put(BigInteger.ONE, a.mod(n));
		
		// se encuentran los primeros 15 rapidamente
		long j=1L;
		BigInteger maxNum = null;
		BigInteger minEulerCoinValue = null;
		
		while (parciales.size()<20) {
			BigInteger num = a.multiply(BigInteger.valueOf(j)).mod(n);
			
			if (esMenor(num, parciales.values())) {
				parciales.put(BigInteger.valueOf(j), num);
				maxNum=BigInteger.valueOf(j);
				minEulerCoinValue=num;
			}
			j++;
		}

		System.out.println(parciales.toString());
		final long limit = minEulerCoinValue.longValue();
		
		for (long i=limit;i>1L;i--) {
			BigInteger Nn = aInv.multiply(BigInteger.valueOf(i)).mod(n);
			
			if ((Nn.compareTo(maxNum)>0) && (Nn.compareTo(n)<0)) {
				parciales.put(Nn, BigInteger.valueOf(i));
				maxNum = Nn;
				minEulerCoinValue = BigInteger.valueOf(i);
			}
		}
		
		System.out.println(parciales.toString());
		System.out.println(parciales.values().stream().reduce(BigInteger.ZERO, BigInteger::add).toString());
	}

	private static boolean esMenor(BigInteger bigInt, Collection<BigInteger> parciales) {
		if (!parciales.isEmpty()) {
			for (BigInteger num : parciales) {
				if (bigInt.compareTo(num)>=0) {return false;}
			}
		}

		return true;
	}
}
